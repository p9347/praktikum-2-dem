const express = require("express");
const app = express();
const mongoose = require("mongoose");
var jwt = require("jsonwebtoken");
const { Timestamp, Double } = require("mongodb");

async function connectDB() {
    await mongoose.connect(
        "mongodb+srv://dem:dem123@cluster0.p6y7h.mongodb.net/?retryWrites=true&w=majority", { useUnifiedTopology: true, useNewUrlParser: true }
    );

    console.log("db connected");
}
connectDB();

// this takes the post body
app.use(express.json({ extended: false }));

app.get("/", (req, res) => res.send("Hello World!"));

// model
var schema = new mongoose.Schema({
    email: "string",
    password: "string"
});
var User = mongoose.model("User", schema);

var DemScema = new mongoose.Schema({
    Datum: Date,
    Elektrarna: String,
    Plan_kWh: String,
    Proizvodnja_kWh: String,
    aRPF_MW: String,
    Srednji_pretok_plan: String

});
var date = new Date();

date.toString();


var FlowRates =  new mongoose.Schema({
    date: String,
    flow_rate: Number
},
{ collection : 'flow_rate' });

var FlowRatesModel = new mongoose.model("flow_rate", FlowRates);


var FlowRatesPlan = new mongoose.Schema({
    date: String,
    flow_rate: Number
},
{ collection : 'flow_rate_plan' })

var FlowRatesPlanModel = new mongoose.model("flow_rate_plan", FlowRatesPlan);

var DemDailyValues = new mongoose.Schema({
    date: String,
    production__kwh: String,
    production_plan__kwh: Number
},
{ collection : 'daily' });

var DemDailyValuesModel = mongoose.model('daily', DemDailyValues);

var DemYearlyValue = new mongoose.Schema({
    production__kwh: Number,
    production_plan__kwh: Number
},
{ collection : 'yearly' });


var novejsiFlow = new mongoose.Schema({
    flow_rate: Number,
    power__mw: Number
},
{ collection : 'flownovo' });

var novejsiFlowModel = new mongoose.model('flownovo', novejsiFlow);


var DemYearlyValueModel = mongoose.model('yearly', DemYearlyValue);

console.log(date);
var DemMonthlyValues = new mongoose.Schema({
    arpf_negative_plan__mw: Number,
    arpf_negative_realization__mw: Number,
    arpf_positive_plan__mw: Number,
    arpf_positive_realization__mw: Number,
    nrrpf_failure_count__unitless: Number,
    nrrpf_success_count__unitless: Number,
    prrpf_failure_count__unitless: Number,
    prrpf_success_count__unitless: Number,
    regulation_q_u_plan__mw: Number,
    regulation_q_u_realization__mw: Number,
    rvf_plan__mw: Number,
    rvf_realization__mw: Number,
    start_from_dark_failure__unitless: Number,
    start_from_dark_success__unitless: Number,
    production__kwh: Number,
    production_plan__kwh: Number
});

var DemMontlyValuesModel = mongoose.model('monthly_value', DemMonthlyValues);

var DemYearlyValues = new mongoose.Schema({
    arpf_negative_plan__mw: Number,
    arpf_negative_realization__mw: Number,
    arpf_positive_plan__mw: Number,
    arpf_positive_realization__mw: Number,
    nrrpf_failure_count__unitless: Number,
    nrrpf_success_count__unitless: Number,
    prrpf_failure_count__unitless: Number,
    prrpf_success_count__unitless: Number,
    regulation_q_u_plan__mw: Number,
    regulation_q_u_realization__mw: Number,
    rvf_plan__mw: Number,
    rvf_realization__mw: Number,
    start_from_dark_failure__unitless: Number,
    start_from_dark_success__unitless: Number,
    production__kwh: Number,
    production_plan__kwh: Number
});

var DemYearlyValuesModel = mongoose.model('yearly_value', DemYearlyValues);

/* var he_mariborskiotok_dailyValue = new mongoose.Schema({
    Datum: Date,
    flow_rate_plan__m3s: String
});

var joint_dem_dailyValue = new mongoose.Schema({
    Datum: Date,
    production__kWh: String,
    production_plan__kwh: String

});

var joint_dem_monthlyValue = new mongoose.Schema({
    Datum: Date,
    production__kWh: String,
    production_plan__kwh: String

});

var joint_dem_yearlyValue = new mongoose.Schema({
    Datum: Date,
    production__kWh: String,
    production_plan__kwh: String

}); */


var DemModel = mongoose.model('dem', DemScema);
/* var mbOtokDailyGraphModel = mongoose.model('jsondem', he_mariborskiotok_dailyValue);
var dailyGraphModel = mongoose.model('jsondem', joint_dem_dailyValue);
var monthlyGraphModel = mongoose.model('jsondem', joint_dem_monthlyValue);
var yearlyGraphModel = mongoose.model('jsondem', joint_dem_yearlyValue);
 */
// signup route api
app.post("/signup", async(req, res) => {
    const { email, password } = req.body;
    console.log(email);

    let user = await User.findOne({ email });

    if (user) {
        return res.json({ msg: "Email already taken" });
    }

    user = new User({
        email,
        password,
    });


    await user.save();
    var token = jwt.sign({ id: user.id }, "password");
    res.json({ token: token });
});
// login route api
app.post("/login", async(req, res) => {
    const { email, password } = req.body;
    console.log(email);

    let user = await User.findOne({ email });
    console.log(user);
    if (!user) {
        return res.json({ msg: "no user found with that email" });
    }
    if (user.password !== password) {
        return res.json({ msg: "password is not correct" });
    }

    var token = jwt.sign({ id: user.id }, "password");
    return res.json({ token: token });
});

app.get('/Vse', (req, res) => {
    DemModel.find().then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
})

app.get('/flowRate', (req, res) => {
    FlowRatesModel.find().then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
})

app.get('/flowRatePlan', (req, res) => {
    FlowRatesPlanModel.find().then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
})

app.get('/Dnevni', (req, res) => {
    DemDailyValuesModel.find().then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
})

app.get('/Mesecni', (req, res) => {
    DemMontlyValuesModel.find().then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
})

app.get('/NoviFlow', (req, res) => {
   
    novejsiFlowModel.find().then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
})

app.get('/Letni', (req, res) => {
    DemYearlyValueModel.find().then((result) => {
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
})

/* app.get('/mariborskiOtokDailyValues', (req, res) => {
    mbOtokDailyGraphModel.find().then((result) => {
        console.log(result);
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
})

app.get('/jointdemDailyValues', (req, res) => {
    dailyGraphModel.find().then((result) => {
        console.log(result);
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
})

app.get('/jointdemMonthlyValues', (req, res) => {
    monthlyGraphModel.find().then((result) => {
        console.log(result);
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
})

app.get('/jointdemYearlyValues', (req, res) => {
    yearlyGraphModel.find().then((result) => {
        console.log(result);
        res.send(result);
    }).catch((err) => {
        console.log(err);
    })
}) */

var podatki = require('./dem.json');

//podatki = JSON.parse(podatki);

/*
function writeData (data){     

    // couple of visual checks if all looking good before writing to db
    console.log('writing');
    console.log(typeof data);
    console.log(data);
    
    db.collection('podatkis').save(data, function(error, record){
    if (error) throw error;
    console.log("data saved");
    
    });
}
writeData(data);
*/

//var SchemaPodatkiS = GenerateSchema.json(podatki);
//var PodatkiSModel = mongoose.model('podatki', DemPodatkiS);

async function sharniJSON() {
    try {
        await PodatkiSModel.insertMany(podatki);
        console.log('Dela!');
        process.exit();
    } catch (e) {
        console.log(e);
        process.exit();
    }
};

//sharniJSON();

app.listen(5005, () => console.log("Example app listening on port 5005!"));