const http = require("https");

const options = {
    "method": "GET",
    "hostname": "microsoft-authenticator.p.rapidapi.com",
    "port": null,
    "path": "/new_v2/",
    "headers": {
        "X-RapidAPI-Host": "microsoft-authenticator.p.rapidapi.com",
        "X-RapidAPI-Key": "fd93cc1056msh36628589d0778cfp1b1a2bjsna7c46e99de36",
        "useQueryString": true
    }
};

const req = http.request(options, function(res) {
    const chunks = [];

    res.on("data", function(chunk) {
        chunks.push(chunk);
    });

    res.on("end", function() {
        const body = Buffer.concat(chunks);
        console.log(body.toString());
    });
});

req.end();