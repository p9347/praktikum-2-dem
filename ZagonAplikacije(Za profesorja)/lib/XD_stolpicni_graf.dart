import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:neki/XD_tabela.dart';
import './XD_dashboard.dart';
import 'package:adobe_xd/page_link.dart';
import './XD_odjava.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'XD_graf.dart';
import 'flowRateGraph.dart';
import 'stolpicniGraph.dart' as Stolpec;
import 'dart:convert';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:http/http.dart' as http;
import 'package:page_transition/page_transition.dart';

class XD_stolpicni_graf extends StatefulWidget {
  XD_stolpicni_graf();

  @override
  _XD_stolpicni_graf createState() => _XD_stolpicni_graf();
}

class _XD_stolpicni_graf extends State<XD_stolpicni_graf> {
  late TooltipBehavior _tooltipBehavior;
  List<TabelaMariborskiOtok> chartData1 = [];

  late Future<TabelaMariborskiOtok> futureTabela;

  Future<TabelaMariborskiOtok> fetchTabela() async {
    final response = await http.get(
        Uri.parse('http://10.0.2.2:5005/Dnevni'));

    var usersData = jsonDecode(response.body);
    List<dynamic> tabelaJSON = List<dynamic>.from(usersData);

    for (int a = 0; a < tabelaJSON.length; a++) {
      chartData1.add(TabelaMariborskiOtok.fromJson(tabelaJSON, a));
      TabelaMariborskiOtok.fromJson(tabelaJSON, a);
    }

    print(chartData1);
    print(chartData1.length);
    return TabelaMariborskiOtok(date: "", production: 0, plan: 0);
  }

  @override
  void initState() {
    futureTabela = fetchTabela();
    _tooltipBehavior = TooltipBehavior(enable: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragUpdate: (details) {
        if (details.delta.direction <= 0) {
          Navigator.push(
              context,
              PageTransition(
                  type: PageTransitionType.leftToRight, child: XD_dashboard()));
        }
      },
      child: Scaffold(
        backgroundColor: const Color(0xffececec),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.fade, child: XD_odjava()));          },
          child: Icon(Icons.logout_rounded),
          backgroundColor: const Color(0xff78b856),
        ),
        body: Stack(
          children: <Widget>[
            Pinned.fromPins(
              Pin(size: 170.0, middle: 0.5026),
              Pin(size: 30.0, start: 70.0),
              child: Text(
                'STOLPIČNI GRAF',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 20,
                  color: const Color(0xff000000),
                  letterSpacing: 0.4,
                ),
                softWrap: false,
              ),
            ),
            Align(
              //alignment: Alignment(0.030, -0.000),
              child: SizedBox(
                width: 380.0,
                height: 520.0,
                child: Graf(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TabelaMariborskiOtok {
  TabelaMariborskiOtok(
      {required this.date, required this.production, required this.plan});

  final String date;
  final double production;
  final double plan;

  factory TabelaMariborskiOtok.fromJson(List<dynamic> parsedJson, int index) {
    return TabelaMariborskiOtok(
      date: parsedJson[index]['date'].toString(),
      production: double.parse(parsedJson[index]['production__kwh']),
      plan: parsedJson[index]['production_plan__kwh'],
    );
  }
}

const String _svg_yly9z6 =
    '<svg viewBox="0.0 0.0 412.0 90.0" ><path  d="M 0 0 L 412 0 L 412 90 L 0 90 L 0 0 Z" fill="#78b856" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_qfpsq3 =
    '<svg viewBox="21.0 30.0 20.0 30.0" ><path transform="translate(8.25, 26.0)" d="M 30.29386138916016 34 L 12.75 19.00000190734863 L 30.29386138916016 4 L 32.75 6.137499809265137 L 17.70614242553711 19.00000190734863 L 32.75 31.86250305175781 L 30.29386138916016 34 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_nuufo =
    '<svg viewBox="360.0 30.0 30.0 30.0" ><path transform="translate(354.0, 24.0)" d="M 16.625 19.75 L 31.125 19.75 L 26.87499809265137 15.49999904632568 L 28.66666603088379 13.70833301544189 L 36 21.04166603088379 L 28.74999809265137 28.29166603088379 L 26.95833206176758 26.5 L 31.20833206176758 22.25 L 16.625 22.25 L 16.625 19.75 Z M 20.62499809265137 6 L 20.62499809265137 8.5 L 8.5 8.5 C 8.5 8.5 8.5 8.5 8.5 8.5 C 8.5 8.5 8.5 8.5 8.5 8.5 L 8.5 33.5 C 8.5 33.5 8.5 33.5 8.5 33.5 C 8.5 33.5 8.5 33.5 8.5 33.5 L 20.62499809265137 33.5 L 20.62499809265137 36 L 8.5 36 C 7.833333015441895 36 7.25 35.75 6.75 35.24999618530273 C 6.25 34.75 6 34.16666412353516 6 33.5 L 6 8.5 C 6 7.833333015441895 6.25 7.25 6.75 6.75 C 7.25 6.25 7.833333015441895 6 8.5 6 L 20.62499809265137 6 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
