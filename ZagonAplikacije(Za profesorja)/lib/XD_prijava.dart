import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'main.dart' as Navbar;
import 'XD_verifikacija.dart' as Verification;
import 'package:flutter_dotenv/flutter_dotenv.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: XD_prijava(),
      routes: {
        "/Login": (context) => XD_prijava(),
        "/Navbar": (context) => LoggedIn(),
        //LandingScreen.id: (context) => LandingScreen(),
      },
    );
  }
}




class XD_prijava extends StatelessWidget {
  bool isLoggedIn = false;
  var email;
  var password;

  XD_prijava({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff1f0f2),
      body: Stack(
        children: <Widget>[
          Pinned.fromPins(
            Pin(start: 43.0, end: 42.0),
            Pin(size: 202.0, middle: 0.5156),
            child: Container(
              decoration: BoxDecoration(
                color: const Color(0xffffffff),
                borderRadius: BorderRadius.circular(40.0),
                boxShadow: [
                  BoxShadow(
                    color: const Color(0x1f000000),
                    offset: Offset(0, 10),
                    blurRadius: 10,
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment(0.014, -0.048),
            child: SizedBox(
              width: 264.0,
              height: 30.0,
              child: Stack(
                children: <Widget>[
                  Pinned.fromPins(
                    Pin(start: 0.0, end: 0.0),
                    Pin(size: 1.0, end: 0.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: const Color(0xffdddddd),
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,

                    child: CupertinoTextField(
                        placeholder: "Email",
                        decoration: BoxDecoration(
                          border: Border(

                            bottom: BorderSide(color: Color(0x52241332)),
                          ),
                        ),
                        onChanged: (value) {
                          email = value;
                        },

                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 16,
                          color: Colors.grey[850]!,
                          letterSpacing: -0.16,
                          height: 1.25,


                        )

                    ),


                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment(0.014, 0.123),
            child: SizedBox(
              width: 264.0,
              height: 30.0,
              child: Stack(
                children: <Widget>[
                  Pinned.fromPins(
                    Pin(start: 0.0, end: 0.0),
                    Pin(size: 1.0, end: 0.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: const Color(0xffdddddd),
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,

                    child: CupertinoTextField(
                        placeholder: "Geslo",
                        obscureText: true,
                        decoration: BoxDecoration(
                          border: Border(

                            bottom: BorderSide(color: Color(0x52241332)),
                          ),
                        ),
                        onChanged: (value) {
                          password = value;
                        },

                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 16,
                          color: Colors.grey[850]!,
                          letterSpacing: -0.16,
                          height: 1.25,


                        )

                    ),


                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment(0.0, -0.608),
            child: Container(
              width: 206.0,
              height: 95.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: const AssetImage('assets/images/dem_logo.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(start: 43.0, end: 42.0),
            Pin(size: 52.0, middle: 0.7851),
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: const Color(0xff78b856),
                    borderRadius: BorderRadius.circular(36.0),
                  ),
                ),TextButton(
                  onPressed:  () async {
                    await login(email, password);
                    Navigator.push(
                      context,
                      new MaterialPageRoute(
                        builder: (context) => new Verification.XD_verifikacija(),
                      ),
                    );
                  },

                  child: Align(
                    child: Text(
                      'PRIJAVA',
                      style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 14,
                        color: const Color(0xffffffff),
                        letterSpacing: 0.056,
                        height: 1.8571428571428572,
                      ),
                      textHeightBehavior:
                      TextHeightBehavior(applyHeightToFirstAscent: false),
                      textAlign: TextAlign.center,
                      softWrap: false,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
login(email, password) async {
  Future main() async {

    await DotEnv().load(fileName: '.env');
    String link = dotenv.get('URL_LOGIN');




    final http.Response response = await http.post(
      //
      Uri.parse(link),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'email': email,
        'password': password,
      }),
    );
    print(response.body);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var parse = jsonDecode(response.body);

    await prefs.setString('token', parse["token"]);
  }
}
class LoggedIn extends StatelessWidget {
  static const String id = "Main";
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Navbar.NavigationExample());
  }
}
