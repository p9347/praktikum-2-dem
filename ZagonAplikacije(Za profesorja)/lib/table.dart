import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'scrollableWidget.dart';
import 'package:http/http.dart' as http;

class Tabela extends StatefulWidget {
  Tabela();
  @override
  _TestPageState createState() => _TestPageState();
}
class _TestPageState extends State<Tabela> {
  List<TabelaPodatki> tabelData = [];
  int? sortColoumnIndex;
  bool isAscending = false;

  late Future<TabelaPodatki> futureTabela;


  Future<TabelaPodatki> fetchTabelaPodatki() async {
    final response = await http.get(Uri.parse('http://10.0.2.2:5005/Dnevni'));

    var usersData = jsonDecode(response.body);
    List<dynamic> tabelaJSON = List<dynamic>.from(usersData);
    //print("aaaa" + response.body);
    print(tabelaJSON.length);

    for (int i = 0; i < tabelaJSON.length; i++) {
      tabelData.add(TabelaPodatki.fromJson(tabelaJSON, i));
      print(i);
      TabelaPodatki.fromJson(tabelaJSON, i);
    }

    return TabelaPodatki(date: "date", production__kwh: 123, production_plan__kwh: 123);
  }

  @override
  void initState() {

    futureTabela = fetchTabelaPodatki();
    super.initState();
  }
  @override
  Widget build(BuildContext context) =>
      Scaffold(
          //appBar: AppBar(title: const Text("dem")),
        body: ScrollableWidget(child:buildDataTable())
      );
  //Center( child: Container(width: 350.0, margin: const EdgeInsets.only(top: 50.0), child: ScrollableWidget(child:

  Widget buildDataTable() {
    final columns = ['Datum', 'Produkcija MWh', 'Plan MWh'];

    return SizedBox(
      //width: 400.0,
        child:DataTable(
          sortAscending: isAscending,
          sortColumnIndex: sortColoumnIndex,
        columns: getColumns(columns),
        rows: getRows(tabelData),
          dividerThickness: 2,
            headingRowColor: MaterialStateColor.resolveWith((states) => const Color.fromRGBO(61, 72, 80, 1.0)),
        headingTextStyle: const TextStyle(
          color: Colors.white,
        ),
        //border: TableBorder.all(),
            //columnSpacing: 5,
        ),
    );
  }

  List<DataColumn> getColumns(List<String> columns) =>
      columns
          .map((String column) =>
          DataColumn(
            label: Text(column),
            onSort: onSort,
          ))
          .toList();

  List<DataRow> getRows(List<TabelaPodatki> tabelData) =>
      tabelData.map((TabelaPodatki podatek) {
        DateTime datum = DateTime.parse(podatek.date);
        final cells = [DateFormat('dd. MM. yyyy').format(datum), (podatek.production__kwh*0.001).toStringAsFixed(2), (podatek.production_plan__kwh*0.001).toStringAsFixed(2)];

        return DataRow(cells: getCells(cells));
      }).toList();

  List<DataCell> getCells(List<dynamic> cells) =>
      cells.map((data) => DataCell(Text('$data'))).toList();

  void onSort(int coloumnIndex, bool ascending) {
    if(coloumnIndex == 1) {
      tabelData.sort((podatek1, podatek2) =>
          compareStevila(ascending, podatek1.production__kwh, podatek2.production__kwh));
    } else if (coloumnIndex == 2) {
      tabelData.sort((podatek1, podatek2) =>
          compareStevila(ascending, podatek1.production_plan__kwh, podatek2.production_plan__kwh));
    }

    setState(() {
      this.sortColoumnIndex = coloumnIndex;
      this.isAscending = ascending;
    });
  }

  int compareStevila(bool ascending, double podatek1, double podatek2) =>
      ascending ? podatek1.compareTo(podatek2) : podatek2.compareTo(podatek1);

  int compareDatumi(bool ascending, DateTime podatek1, DateTime podatek2) =>
      ascending ? podatek1.compareTo(podatek2) : podatek2.compareTo(podatek1);
}

class TabelaPodatki {
  TabelaPodatki({required this.date, required this.production__kwh, required this.production_plan__kwh});

  final String date;
  final double production__kwh;
  final double production_plan__kwh;


  factory TabelaPodatki.fromJson(List<dynamic> parsedJson, int index) {
    return TabelaPodatki(
      date: parsedJson[index]['date'].toString(),
      production__kwh: double.parse(parsedJson[index]['production__kwh']),
      production_plan__kwh: parsedJson[index]['production_plan__kwh'],
    );
  }
}
