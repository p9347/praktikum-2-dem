import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'XD_odjava.dart';
import 'scrollableWidget.dart';
import 'package:http/http.dart' as http;
import 'package:adobe_xd/pinned.dart';
import './XD_dashboard.dart';
import './XD_graf.dart';
import 'package:page_transition/page_transition.dart';

class XD_tabela extends StatefulWidget {
  XD_tabela();

  @override
  _XD_tabela createState() => _XD_tabela();
}

class _XD_tabela extends State<XD_tabela> {
  List<TabelaPodatki> tabelData = [];
  int? sortColoumnIndex;
  bool isAscending = false;

  late Future<TabelaPodatki> futureTabela;

  Future<TabelaPodatki> fetchTabelaPodatki() async {
    final response = await http.get(
        Uri.parse('http://10.0.2.2:5005/Dnevni'));

    var usersData = jsonDecode(response.body);
    List<dynamic> tabelaJSON = List<dynamic>.from(usersData);
    //print("aaaa" + response.body);
    print(tabelaJSON.length);

    for (int i = 0; i < tabelaJSON.length; i++) {
      tabelData.add(TabelaPodatki.fromJson(tabelaJSON, i));
      print(i);
      TabelaPodatki.fromJson(tabelaJSON, i);
    }

    return TabelaPodatki(
        date: "date", production__kwh: 123, production_plan__kwh: 123);
  }

  @override
  void initState() {
    futureTabela = fetchTabelaPodatki();
    super.initState();
  }

  //body: ScrollableWidget(child:buildDataTable())
  @override
  Widget build(BuildContext context) => GestureDetector(
        onVerticalDragUpdate: (details) {
          Navigator.push(
              context,
              PageTransition(
                  type: PageTransitionType.topToBottom, child: XD_graf()));
        },
        onHorizontalDragUpdate: (details) {
          if (details.delta.direction > 0) {
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.rightToLeft,
                    child: XD_dashboard()));
          }
          if (details.delta.direction <= 0) {
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.leftToRight,
                    child: XD_dashboard()));
          }
        },
        child: Scaffold(
          backgroundColor: const Color(0xffececec),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.fade, child: XD_odjava()));          },
            child: Icon(Icons.logout_rounded),
            backgroundColor: const Color(0xff78b856),
          ),
          body: Stack(
            children: <Widget>[
              Pinned.fromPins(
                Pin(size: 250.0, middle: 0.5026),
                Pin(size: 30.0, start: 70.0),
                child: Text(
                  'TABELA VSE ELEKTRARNE',
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 20,
                    color: const Color(0xff000000),
                    letterSpacing: 0.4,
                  ),
                  softWrap: false,
                ),
              ),
              Align(
                alignment: Alignment(0.006, -0.037),
                child: SizedBox(
                    //width: 380.0,
                    height: 513.0,
                    child: ScrollableWidget(child: buildDataTable())),
              ),
            ],
          ),
        ),
      );

  //Center( child: Container(width: 350.0, margin: const EdgeInsets.only(top: 50.0), child: ScrollableWidget(child:

  Widget buildDataTable() {
    final columns = ['Datum', 'Produkcija MWh', 'Plan MWh'];

    return SizedBox(
      //width: 400.0,
      child: DataTable(
        sortAscending: isAscending,

        sortColumnIndex: sortColoumnIndex,

        columns: getColumns(columns),

        rows: getRows(tabelData),

        dividerThickness: 2,

        headingRowColor: MaterialStateColor.resolveWith(
            (states) => const Color.fromRGBO(61, 72, 80, 1.0)),

        headingTextStyle: const TextStyle(
          color: Colors.white,
        ),

        //border: TableBorder.all(),
        //columnSpacing: 5,
      ),
    );
  }

  List<DataColumn> getColumns(List<String> columns) => columns
      .map((String column) => DataColumn(
            label: Text(column),
            onSort: onSort,
          ))
      .toList();

  List<DataRow> getRows(List<TabelaPodatki> tabelData) =>
      tabelData.map((TabelaPodatki podatek) {
        DateTime datum = DateTime.parse(podatek.date);
        final cells = [
          DateFormat('dd. MM. yyyy').format(datum),
          (podatek.production__kwh * 0.001).toStringAsFixed(2),
          (podatek.production_plan__kwh * 0.001).toStringAsFixed(2)
        ];

        return DataRow(cells: getCells(cells));
      }).toList();

  List<DataCell> getCells(List<dynamic> cells) =>
      cells.map((data) => DataCell(Text('$data'))).toList();

  void onSort(int coloumnIndex, bool ascending) {
    if (coloumnIndex == 1) {
      tabelData.sort((podatek1, podatek2) => compareStevila(
          ascending, podatek1.production__kwh, podatek2.production__kwh));
    } else if (coloumnIndex == 2) {
      tabelData.sort((podatek1, podatek2) => compareStevila(ascending,
          podatek1.production_plan__kwh, podatek2.production_plan__kwh));
    }

    setState(() {
      this.sortColoumnIndex = coloumnIndex;
      this.isAscending = ascending;
    });
  }

  int compareStevila(bool ascending, double podatek1, double podatek2) =>
      ascending ? podatek1.compareTo(podatek2) : podatek2.compareTo(podatek1);

  int compareDatumi(bool ascending, DateTime podatek1, DateTime podatek2) =>
      ascending ? podatek1.compareTo(podatek2) : podatek2.compareTo(podatek1);
}

class TabelaPodatki {
  TabelaPodatki(
      {required this.date,
      required this.production__kwh,
      required this.production_plan__kwh});

  final String date;
  final double production__kwh;
  final double production_plan__kwh;

  factory TabelaPodatki.fromJson(List<dynamic> parsedJson, int index) {
    return TabelaPodatki(
      date: parsedJson[index]['date'].toString(),
      production__kwh: double.parse(parsedJson[index]['production__kwh']),
      production_plan__kwh: parsedJson[index]['production_plan__kwh'],
    );
  }
}

const String _svg_yly9z6 =
    '<svg viewBox="0.0 0.0 412.0 90.0" ><path  d="M 0 0 L 412 0 L 412 90 L 0 90 L 0 0 Z" fill="#78b856" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_qfpsq3 =
    '<svg viewBox="21.0 30.0 20.0 30.0" ><path transform="translate(8.25, 26.0)" d="M 30.29386138916016 34 L 12.75 19.00000190734863 L 30.29386138916016 4 L 32.75 6.137499809265137 L 17.70614242553711 19.00000190734863 L 32.75 31.86250305175781 L 30.29386138916016 34 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_nuufo =
    '<svg viewBox="360.0 30.0 30.0 30.0" ><path transform="translate(354.0, 24.0)" d="M 16.625 19.75 L 31.125 19.75 L 26.87499809265137 15.49999904632568 L 28.66666603088379 13.70833301544189 L 36 21.04166603088379 L 28.74999809265137 28.29166603088379 L 26.95833206176758 26.5 L 31.20833206176758 22.25 L 16.625 22.25 L 16.625 19.75 Z M 20.62499809265137 6 L 20.62499809265137 8.5 L 8.5 8.5 C 8.5 8.5 8.5 8.5 8.5 8.5 C 8.5 8.5 8.5 8.5 8.5 8.5 L 8.5 33.5 C 8.5 33.5 8.5 33.5 8.5 33.5 C 8.5 33.5 8.5 33.5 8.5 33.5 L 20.62499809265137 33.5 L 20.62499809265137 36 L 8.5 36 C 7.833333015441895 36 7.25 35.75 6.75 35.24999618530273 C 6.25 34.75 6 34.16666412353516 6 33.5 L 6 8.5 C 6 7.833333015441895 6.25 7.25 6.75 6.75 C 7.25 6.25 7.833333015441895 6 8.5 6 L 20.62499809265137 6 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
