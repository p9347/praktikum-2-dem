import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:neki/XD_stolpicni_graf.dart';
import 'package:adobe_xd/page_link.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:page_transition/page_transition.dart';

import 'XD_graf.dart';
import 'XD_odjava.dart';
import 'XD_tabela.dart';

class XD_dashboard extends StatelessWidget {
  XD_dashboard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragUpdate: (details) {
        if (details.delta.direction > 0) {
          Navigator.push(
              context,
              PageTransition(
                  type: PageTransitionType.rightToLeft, child: XD_graf()));
        }
        if (details.delta.direction <= 0) {
          Navigator.push(
              context,
              PageTransition(
                  type: PageTransitionType.leftToRight, child: XD_graf()));
        }
      },
      child: Scaffold(
        backgroundColor: const Color(0xffececec),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                PageTransition(
                    type: PageTransitionType.fade, child: XD_odjava()));          },
          child: Icon(Icons.logout_rounded),
          backgroundColor: const Color(0xff78b856),
        ),
        body: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment(-0.082, 1.0),
              child: SizedBox(
                width: 314.0,
                height: 590.0,
                child: SingleChildScrollView(
                  primary: false,
                  child: SizedBox(
                    width: 314.0,
                    height: 621.0,
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 50),
                          child: Stack(
                            children: <Widget>[
                              Pinned.fromPins(
                                Pin(start: 0.0, end: 0.0),
                                Pin(size: 134.9, start: 0.0),
                                child: Stack(
                                  children: <Widget>[
                                    SizedBox.expand(
                                        child: SvgPicture.string(
                                      _svg_le72,
                                      allowDrawingOutsideViewBox: true,
                                      fit: BoxFit.fill,
                                    )),
                                    Pinned.fromPins(
                                      Pin(size: 109.0, middle: 0.5024),
                                      Pin(size: 21.0, start: 12.0),
                                      child: Text(
                                        'HE Dravograd',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 15,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.3,
                                          fontWeight: FontWeight.w700,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Pinned.fromPins(
                                      Pin(size: 26.0, middle: 0.5),
                                      Pin(size: 28.0, end: 18.4),
                                      child: Text(
                                        '141',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 16,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.4,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Pinned.fromPins(
                                      Pin(size: 30.0, start: 37.0),
                                      Pin(size: 28.0, middle: 0.8045),
                                      child: Text(
                                        '123',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 16,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.4,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Pinned.fromPins(
                                      Pin(size: 53.0, start: 30.0),
                                      Pin(size: 17.0, middle: 0.4835),
                                      child: Text(
                                        'Moč (W)',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 12,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.24,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.052, -0.033),
                                      child: SizedBox(
                                        width: 82.0,
                                        height: 17.0,
                                        child: Text.rich(
                                          TextSpan(
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 12,
                                              color: const Color(0xff707070),
                                              letterSpacing: 0.24,
                                            ),
                                            children: [
                                              TextSpan(
                                                text: 'Pretok (m',
                                              ),
                                              TextSpan(
                                                text: '3',
                                              ),
                                              TextSpan(
                                                text: '/s)',
                                              ),
                                            ],
                                          ),
                                          textHeightBehavior:
                                              TextHeightBehavior(
                                                  applyHeightToFirstAscent:
                                                      false),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.666, -0.033),
                                      child: SizedBox(
                                        width: 27.0,
                                        height: 17.0,
                                        child: Text(
                                          'Graf',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 12,
                                            color: const Color(0xff707070),
                                            letterSpacing: 0.24,
                                          ),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.656, 0.582),
                                      child: SizedBox(
                                        width: 23.0,
                                        height: 23.0,
                                        child: PageLink(
                                          links: [
                                            PageLinkInfo(
                                              transition: LinkTransition.Fade,
                                              ease: Curves.easeOut,
                                              duration: 0.3,
                                              pageBuilder: () =>
                                                  XD_stolpicni_graf(),
                                            ),
                                          ],
                                          child: Stack(
                                            children: <Widget>[
                                              Stack(
                                                children: <Widget>[
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              9.0),
                                                      border: Border.all(
                                                          width: 1.0,
                                                          color: const Color(
                                                              0xff707070)),
                                                    ),
                                                  ),
                                                  Pinned.fromPins(
                                                    Pin(start: 4.0, end: 4.0),
                                                    Pin(
                                                        size: 10.0,
                                                        middle: 0.5),
                                                    child: SvgPicture.string(
                                                      _svg_btiwkv,
                                                      allowDrawingOutsideViewBox:
                                                          true,
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Pinned.fromPins(
                                Pin(start: 0.0, end: 0.0),
                                Pin(size: 134.9, middle: 0.3333),
                                child: Stack(
                                  children: <Widget>[
                                    SizedBox.expand(
                                        child: SvgPicture.string(
                                      _svg_le72,
                                      allowDrawingOutsideViewBox: true,
                                      fit: BoxFit.fill,
                                    )),
                                    Pinned.fromPins(
                                      Pin(size: 68.0, middle: 0.5),
                                      Pin(size: 21.0, start: 10.9),
                                      child: Text(
                                        'HE Labot',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 15,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.3,
                                          fontWeight: FontWeight.w700,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.0, 0.609),
                                      child: SizedBox(
                                        width: 26.0,
                                        height: 28.0,
                                        child: Text(
                                          '141',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 16,
                                            color: const Color(0xff707070),
                                            letterSpacing: 0.4,
                                          ),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Pinned.fromPins(
                                      Pin(size: 30.0, start: 37.0),
                                      Pin(size: 28.0, middle: 0.8045),
                                      child: Text(
                                        '123',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 16,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.4,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Pinned.fromPins(
                                      Pin(size: 53.0, start: 30.0),
                                      Pin(size: 17.0, middle: 0.4835),
                                      child: Text(
                                        'Moč (W)',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 12,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.24,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.052, -0.033),
                                      child: SizedBox(
                                        width: 82.0,
                                        height: 17.0,
                                        child: Text.rich(
                                          TextSpan(
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 12,
                                              color: const Color(0xff707070),
                                              letterSpacing: 0.24,
                                            ),
                                            children: [
                                              TextSpan(
                                                text: 'Pretok (m',
                                              ),
                                              TextSpan(
                                                text: '3',
                                              ),
                                              TextSpan(
                                                text: '/s)',
                                              ),
                                            ],
                                          ),
                                          textHeightBehavior:
                                              TextHeightBehavior(
                                                  applyHeightToFirstAscent:
                                                      false),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.666, -0.033),
                                      child: SizedBox(
                                        width: 27.0,
                                        height: 17.0,
                                        child: Text(
                                          'Graf',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 12,
                                            color: const Color(0xff707070),
                                            letterSpacing: 0.24,
                                          ),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.656, 0.582),
                                      child: SizedBox(
                                        width: 23.0,
                                        height: 23.0,
                                        child: PageLink(
                                          links: [
                                            PageLinkInfo(
                                              transition: LinkTransition.Fade,
                                              ease: Curves.easeOut,
                                              duration: 0.3,
                                              pageBuilder: () =>
                                                  XD_stolpicni_graf(),
                                            ),
                                          ],
                                          child: Stack(
                                            children: <Widget>[
                                              Stack(
                                                children: <Widget>[
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              9.0),
                                                      border: Border.all(
                                                          width: 1.0,
                                                          color: const Color(
                                                              0xff707070)),
                                                    ),
                                                  ),
                                                  Pinned.fromPins(
                                                    Pin(start: 4.0, end: 4.0),
                                                    Pin(
                                                        size: 10.0,
                                                        middle: 0.5),
                                                    child: SvgPicture.string(
                                                      _svg_btiwkv,
                                                      allowDrawingOutsideViewBox:
                                                          true,
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Pinned.fromPins(
                                Pin(start: 0.0, end: 0.0),
                                Pin(size: 134.9, middle: 0.6667),
                                child: Stack(
                                  children: <Widget>[
                                    SizedBox.expand(
                                        child: SvgPicture.string(
                                      _svg_le72,
                                      allowDrawingOutsideViewBox: true,
                                      fit: BoxFit.fill,
                                    )),
                                    Pinned.fromPins(
                                      Pin(size: 148.0, middle: 0.5241),
                                      Pin(size: 21.0, start: 16.9),
                                      child: Text(
                                        'HE Mariborski otok',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 15,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.3,
                                          fontWeight: FontWeight.w700,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.0, 0.609),
                                      child: SizedBox(
                                        width: 26.0,
                                        height: 28.0,
                                        child: Text(
                                          '141',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 16,
                                            color: const Color(0xff707070),
                                            letterSpacing: 0.4,
                                          ),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Pinned.fromPins(
                                      Pin(size: 30.0, start: 37.0),
                                      Pin(size: 28.0, middle: 0.8045),
                                      child: Text(
                                        '123',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 16,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.4,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Pinned.fromPins(
                                      Pin(size: 53.0, start: 30.0),
                                      Pin(size: 17.0, middle: 0.4835),
                                      child: Text(
                                        'Moč (W)',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 12,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.24,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.052, -0.033),
                                      child: SizedBox(
                                        width: 82.0,
                                        height: 17.0,
                                        child: Text.rich(
                                          TextSpan(
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 12,
                                              color: const Color(0xff707070),
                                              letterSpacing: 0.24,
                                            ),
                                            children: [
                                              TextSpan(
                                                text: 'Pretok (m',
                                              ),
                                              TextSpan(
                                                text: '3',
                                              ),
                                              TextSpan(
                                                text: '/s)',
                                              ),
                                            ],
                                          ),
                                          textHeightBehavior:
                                              TextHeightBehavior(
                                                  applyHeightToFirstAscent:
                                                      false),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.666, -0.033),
                                      child: SizedBox(
                                        width: 27.0,
                                        height: 17.0,
                                        child: Text(
                                          'Graf',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 12,
                                            color: const Color(0xff707070),
                                            letterSpacing: 0.24,
                                          ),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.656, 0.582),
                                      child: SizedBox(
                                        width: 23.0,
                                        height: 23.0,
                                        child: PageLink(
                                          links: [
                                            PageLinkInfo(
                                              transition: LinkTransition.Fade,
                                              ease: Curves.easeOut,
                                              duration: 0.3,
                                              pageBuilder: () =>
                                                  XD_stolpicni_graf(),
                                            ),
                                          ],
                                          child: Stack(
                                            children: <Widget>[
                                              Stack(
                                                children: <Widget>[
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              9.0),
                                                      border: Border.all(
                                                          width: 1.0,
                                                          color: const Color(
                                                              0xff707070)),
                                                    ),
                                                  ),
                                                  Pinned.fromPins(
                                                    Pin(start: 4.0, end: 4.0),
                                                    Pin(
                                                        size: 10.0,
                                                        middle: 0.5),
                                                    child: SvgPicture.string(
                                                      _svg_btiwkv,
                                                      allowDrawingOutsideViewBox:
                                                          true,
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Pinned.fromPins(
                                Pin(start: 0.0, end: 0.0),
                                Pin(size: 134.9, end: 0.0),
                                child: Stack(
                                  children: <Widget>[
                                    SizedBox.expand(
                                        child: SvgPicture.string(
                                      _svg_le72,
                                      allowDrawingOutsideViewBox: true,
                                      fit: BoxFit.fill,
                                    )),
                                    Pinned.fromPins(
                                      Pin(size: 79.0, middle: 0.5362),
                                      Pin(size: 21.0, start: 14.9),
                                      child: Text(
                                        'HE Formin',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 15,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.3,
                                          fontWeight: FontWeight.w700,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.0, 0.609),
                                      child: SizedBox(
                                        width: 26.0,
                                        height: 28.0,
                                        child: Text(
                                          '141',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 16,
                                            color: const Color(0xff707070),
                                            letterSpacing: 0.4,
                                          ),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Pinned.fromPins(
                                      Pin(size: 30.0, start: 37.0),
                                      Pin(size: 28.0, middle: 0.8045),
                                      child: Text(
                                        '123',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 16,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.4,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Pinned.fromPins(
                                      Pin(size: 53.0, start: 30.0),
                                      Pin(size: 17.0, middle: 0.4835),
                                      child: Text(
                                        'Moč (W)',
                                        style: TextStyle(
                                          fontFamily: 'Poppins',
                                          fontSize: 12,
                                          color: const Color(0xff707070),
                                          letterSpacing: 0.24,
                                        ),
                                        softWrap: false,
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.052, -0.033),
                                      child: SizedBox(
                                        width: 82.0,
                                        height: 17.0,
                                        child: Text.rich(
                                          TextSpan(
                                            style: TextStyle(
                                              fontFamily: 'Poppins',
                                              fontSize: 12,
                                              color: const Color(0xff707070),
                                              letterSpacing: 0.24,
                                            ),
                                            children: [
                                              TextSpan(
                                                text: 'Pretok (m',
                                              ),
                                              TextSpan(
                                                text: '3',
                                              ),
                                              TextSpan(
                                                text: '/s)',
                                              ),
                                            ],
                                          ),
                                          textHeightBehavior:
                                              TextHeightBehavior(
                                                  applyHeightToFirstAscent:
                                                      false),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.666, -0.033),
                                      child: SizedBox(
                                        width: 27.0,
                                        height: 17.0,
                                        child: Text(
                                          'Graf',
                                          style: TextStyle(
                                            fontFamily: 'Poppins',
                                            fontSize: 12,
                                            color: const Color(0xff707070),
                                            letterSpacing: 0.24,
                                          ),
                                          softWrap: false,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment(0.656, 0.582),
                                      child: SizedBox(
                                        width: 23.0,
                                        height: 23.0,
                                        child: PageLink(
                                          links: [
                                            PageLinkInfo(
                                              transition: LinkTransition.Fade,
                                              ease: Curves.easeOut,
                                              duration: 0.3,
                                              pageBuilder: () =>
                                                  XD_stolpicni_graf(),
                                            ),
                                          ],
                                          child: Stack(
                                            children: <Widget>[
                                              Stack(
                                                children: <Widget>[
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              9.0),
                                                      border: Border.all(
                                                          width: 1.0,
                                                          color: const Color(
                                                              0xff707070)),
                                                    ),
                                                  ),
                                                  Pinned.fromPins(
                                                    Pin(start: 4.0, end: 4.0),
                                                    Pin(
                                                        size: 10.0,
                                                        middle: 0.5),
                                                    child: SvgPicture.string(
                                                      _svg_btiwkv,
                                                      allowDrawingOutsideViewBox:
                                                          true,
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Pinned.fromPins(
              Pin(start: 0.0, end: 0.0),
              Pin(size: 188.0, start: 0.0),
              child: SvgPicture.string(
                _svg_kahfy2,
                allowDrawingOutsideViewBox: true,
                fit: BoxFit.fill,
              ),
            ),
            Pinned.fromPins(
              Pin(size: 234.0, middle: 0.4775),
              Pin(size: 35.0, start: 94.0),
              child: Text(
                'DNEVNO POROČILO',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 25,
                  color: const Color(0xfffafafa),
                ),
                softWrap: false,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

const String _svg_le72 =
    '<svg viewBox="216.1 381.0 314.0 134.9" ><path transform="translate(216.13, 381.0)" d="M 10 0 L 304 0 C 309.5228576660156 0 314 2.745286703109741 314 6.131769180297852 L 314 128.7671508789062 C 314 132.1536407470703 309.5228576660156 134.89892578125 304 134.89892578125 L 10 134.89892578125 C 4.477152347564697 134.89892578125 0 132.1536407470703 0 128.7671508789062 L 0 6.131769180297852 C 0 2.745286703109741 4.477152347564697 0 10 0 Z" fill="#f5f5f5" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_kahfy2 =
    '<svg viewBox="0.0 0.0 412.0 188.0" ><path  d="M 0 0 L 412 0 L 412 158 C 412 174.5685424804688 398.5685424804688 188 382 188 L 30 188 C 13.43145751953125 188 0 174.5685424804688 0 158 L 0 0 Z" fill="#78b856" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_btiwkv =
    '<svg viewBox="672.1 913.9 17.9 18.3" ><path transform="translate(662.07, 903.87)" d="M 10.00000095367432 28.253662109375 L 10.00000095367432 14.86764335632324 L 13.19068908691406 14.86764335632324 L 13.19068908691406 28.253662109375 L 10.00000095367432 28.253662109375 Z M 17.33858489990234 28.253662109375 L 17.33858489990234 10 L 20.52927207946777 10 L 20.52927207946777 28.253662109375 L 17.33858489990234 28.253662109375 Z M 24.67716979980469 28.253662109375 L 24.67716979980469 19.73528671264648 L 27.86785888671875 19.73528671264648 L 27.86785888671875 28.253662109375 L 24.67716979980469 28.253662109375 Z" fill="#4f5f6a" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
