import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'prijava.dart';
import 'package:adobe_xd/page_link.dart';
import 'package:flutter_svg/flutter_svg.dart';

class pozdravljeni extends StatelessWidget {
  pozdravljeni({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff1f0f2),
      body: Stack(
        children: <Widget>[
          Container(),
          Container(
            color: const Color(0xff000000),
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 124.0),
          ),
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: const AssetImage('assets/images/dem_elek.jpg'),
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.83), BlendMode.dstIn),
              ),
            ),
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 124.0),
          ),
          Pinned.fromPins(
            Pin(start: 32.0, end: 0.0),
            Pin(size: 224.0, end: 21.0),
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: const Color(0xff78b856),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      bottomLeft: Radius.circular(80.0),
                    ),
                  ),
                  margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 28.0),
                ),
                Align(
                  alignment: Alignment.bottomRight,
                  child: SizedBox(
                    width: 173.0,
                    height: 56.0,
                    child: PageLink(
                      links: [
                        PageLinkInfo(
                          transition: LinkTransition.Fade,
                          ease: Curves.easeOut,
                          duration: 0.3,
                          pageBuilder: () => prijava(),
                        ),
                      ],
                      child: Stack(
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                  color: const Color(0xff3b4d58),
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(50.0),
                                    bottomLeft: Radius.circular(50.0),
                                  ),
                                ),
                              ),
                              Pinned.fromPins(
                                Pin(startFraction: 0.2775, endFraction: 0.3988),
                                Pin(size: 20.0, middle: 0.5278),
                                child: Text(
                                  'PRIJAVA',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 14,
                                    color: const Color(0xffffffff),
                                    letterSpacing: 0.056,
                                    height: 1.4285714285714286,
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  textAlign: TextAlign.center,
                                  softWrap: false,
                                ),
                              ),
                              Pinned.fromPins(
                                Pin(size: 13.0, end: 24.0),
                                Pin(size: 13.0, middle: 0.5116),
                                child: SvgPicture.string(
                                  _svg_wc3z2r,
                                  allowDrawingOutsideViewBox: true,
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Pinned.fromPins(
                  Pin(start: 48.0, end: 2.0),
                  Pin(size: 78.0, middle: 0.4521),
                  child: Text(
                    'Pozdravljeni!',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 32,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                      height: 1.25,
                    ),
                    textHeightBehavior:
                        TextHeightBehavior(applyHeightToFirstAscent: false),
                  ),
                ),
                Pinned.fromPins(
                  Pin(startFraction: 0.1395, endFraction: 0.4079),
                  Pin(size: 16.0, middle: 0.1875),
                  child: Text(
                    'Dravske elektrarne Maribor',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 11,
                      color: const Color(0xb2ffffff),
                      letterSpacing: 0.9999999694824219,
                      height: 1.8181818181818181,
                    ),
                    textHeightBehavior:
                        TextHeightBehavior(applyHeightToFirstAscent: false),
                    softWrap: false,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

const String _svg_wc3z2r =
    '<svg viewBox="136.0 22.0 13.0 13.0" ><path transform="translate(136.0, 22.0)" d="M 6.5 0 L 13 6.5 L 6.5 13 L 5.351796627044678 11.85179710388184 L 9.886227607727051 7.297904491424561 L 0 7.297904491424561 L 0 5.702095985412598 L 9.886227607727051 5.702095985412598 L 5.351796627044678 1.148203611373901 L 6.5 0 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="10" stroke-linecap="butt" /></svg>';
