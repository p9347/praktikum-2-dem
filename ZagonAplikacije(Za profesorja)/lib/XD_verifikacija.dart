import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import './XD_dashboard.dart';
import 'package:adobe_xd/page_link.dart';
import 'package:flutter_svg/flutter_svg.dart';

class XD_verifikacija extends StatelessWidget {
  XD_verifikacija({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffececec),
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment(0.0, -0.132),
            child: SizedBox(
              width: 286.0,
              height: 91.0,
              child: Text.rich(
                TextSpan(
                  style: TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 15,
                    color: const Color(0xff3c4e59),
                    height: 2.3333333333333335,
                  ),
                  children: [
                    TextSpan(
                      text: 'VERIFIKACIJA PROFILA\n',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    TextSpan(
                      text:
                          'Vnesite verifikacijsko kodo iz aplikacije\nMicrosoft Authenticator.',
                    ),
                  ],
                ),
                textHeightBehavior:
                    TextHeightBehavior(applyHeightToFirstAscent: false),
                textAlign: TextAlign.center,
                softWrap: false,
              ),
            ),
          ),
          Pinned.fromPins(
            Pin(start: 18.4, end: 18.4),
            Pin(size: 50.9, middle: 0.5893),
            child: Stack(
              children: <Widget>[
                Pinned.fromPins(
                  Pin(size: 49.9, middle: 0.8),
                  Pin(start: 0.0, end: 0.0),
                  child: SvgPicture.string(
                    _svg_v4bs1,
                    allowDrawingOutsideViewBox: true,
                    fit: BoxFit.fill,
                  ),
                ),
                Pinned.fromPins(
                  Pin(size: 49.9, end: 0.0),
                  Pin(start: 0.0, end: 0.0),
                  child: SvgPicture.string(
                    _svg_oejcx2,
                    allowDrawingOutsideViewBox: true,
                    fit: BoxFit.fill,
                  ),
                ),
                Pinned.fromPins(
                  Pin(size: 49.9, middle: 0.4),
                  Pin(start: 0.0, end: 0.0),
                  child: SvgPicture.string(
                    _svg_tt58m9,
                    allowDrawingOutsideViewBox: true,
                    fit: BoxFit.fill,
                  ),
                ),
                Pinned.fromPins(
                  Pin(size: 49.9, middle: 0.6),
                  Pin(start: 0.0, end: 0.0),
                  child: SvgPicture.string(
                    _svg_vl3v,
                    allowDrawingOutsideViewBox: true,
                    fit: BoxFit.fill,
                  ),
                ),
                Pinned.fromPins(
                  Pin(size: 49.9, start: 0.0),
                  Pin(start: 0.0, end: 0.0),
                  child: SvgPicture.string(
                    _svg_tlzbys,
                    allowDrawingOutsideViewBox: true,
                    fit: BoxFit.fill,
                  ),
                ),
                Pinned.fromPins(
                  Pin(size: 49.9, middle: 0.2),
                  Pin(start: 0.0, end: 0.0),
                  child: SvgPicture.string(
                    _svg_fphmh4,
                    allowDrawingOutsideViewBox: true,
                    fit: BoxFit.fill,
                  ),
                ),
              ],
            ),
          ),
          Pinned.fromPins(
            Pin(startFraction: 0.2767, endFraction: 0.4757),
            Pin(size: 14.0, middle: 0.676),
            child: Text(
              'Kode nisem prejel.',
              style: TextStyle(
                fontFamily: 'Montserrat-Medium',
                fontSize: 10.856976509094238,
                color: const Color(0x8078b856),
                height: 1.53846156548916,
              ),
              textHeightBehavior:
                  TextHeightBehavior(applyHeightToFirstAscent: false),
              textAlign: TextAlign.center,
              softWrap: false,
            ),
          ),
          Pinned.fromPins(
            Pin(start: 42.5, end: 42.5),
            Pin(size: 52.0, middle: 0.779),
            child: PageLink(
              links: [
                PageLinkInfo(
                  transition: LinkTransition.Fade,
                  ease: Curves.easeOut,
                  duration: 0.3,
                  pageBuilder: () => XD_dashboard(),
                ),
              ],
              child: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: const Color(0xff78b856),
                      borderRadius: BorderRadius.circular(38.0),
                      border: Border.all(
                          width: 1.0, color: const Color(0xff78b856)),
                    ),
                  ),
                  Align(
                    alignment: Alignment(0.004, 0.0),
                    child: SizedBox(
                      width: 68.0,
                      height: 20.0,
                      child: Text(
                        'NADALJUJ',
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 14,
                          color: const Color(0xffffffff),
                          height: 1.1930743626185827,
                        ),
                        textHeightBehavior:
                            TextHeightBehavior(applyHeightToFirstAscent: false),
                        textAlign: TextAlign.center,
                        softWrap: false,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment(0.348, 0.35),
            child: SizedBox(
              width: 84.0,
              height: 16.0,
              child: Text(
                'Pošlji še enkrat',
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontSize: 11,
                  color: const Color(0xff3c4e59),
                  letterSpacing: 0.22,
                ),
                softWrap: false,
              ),
            ),
          ),
          Align(
            alignment: Alignment(0.0, -0.608),
            child: Container(
              width: 206.0,
              height: 95.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: const AssetImage('assets/images/dem_logo.png'),
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

const String _svg_v4bs1 =
    '<svg viewBox="260.2 0.0 49.9 50.9" ><path transform="translate(260.21, 0.0)" d="M 21 0 L 28.87411117553711 0 C 40.47209167480469 0 49.87411117553711 9.402019500732422 49.87411117553711 21 L 49.87411117553711 29.93230819702148 C 49.87411117553711 41.53028869628906 40.47209167480469 50.93230819702148 28.87411117553711 50.93230819702148 L 21 50.93230819702148 C 9.402019500732422 50.93230819702148 0 41.53028869628906 0 29.93230819702148 L 0 21 C 0 9.402019500732422 9.402019500732422 0 21 0 Z" fill="none" stroke="#ced9cc" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_oejcx2 =
    '<svg viewBox="325.3 0.0 49.9 50.9" ><path transform="translate(325.27, 0.0)" d="M 21 0 L 28.87411117553711 0 C 40.47209167480469 0 49.87411117553711 9.402019500732422 49.87411117553711 21 L 49.87411117553711 29.93230819702148 C 49.87411117553711 41.53028869628906 40.47209167480469 50.93230819702148 28.87411117553711 50.93230819702148 L 21 50.93230819702148 C 9.402019500732422 50.93230819702148 0 41.53028869628906 0 29.93230819702148 L 0 21 C 0 9.402019500732422 9.402019500732422 0 21 0 Z" fill="none" stroke="#ced9cc" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_tt58m9 =
    '<svg viewBox="130.1 0.0 49.9 50.9" ><path transform="translate(130.11, 0.0)" d="M 21 0 L 28.87411117553711 0 C 40.47209167480469 0 49.87411117553711 9.402019500732422 49.87411117553711 21 L 49.87411117553711 29.93230819702148 C 49.87411117553711 41.53028869628906 40.47209167480469 50.93230819702148 28.87411117553711 50.93230819702148 L 21 50.93230819702148 C 9.402019500732422 50.93230819702148 0 41.53028869628906 0 29.93230819702148 L 0 21 C 0 9.402019500732422 9.402019500732422 0 21 0 Z" fill="none" stroke="#ced9cc" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_vl3v =
    '<svg viewBox="195.2 0.0 49.9 50.9" ><path transform="translate(195.16, 0.0)" d="M 21 0 L 28.87411117553711 0 C 40.47209167480469 0 49.87411117553711 9.402019500732422 49.87411117553711 21 L 49.87411117553711 29.93230819702148 C 49.87411117553711 41.53028869628906 40.47209167480469 50.93230819702148 28.87411117553711 50.93230819702148 L 21 50.93230819702148 C 9.402019500732422 50.93230819702148 0 41.53028869628906 0 29.93230819702148 L 0 21 C 0 9.402019500732422 9.402019500732422 0 21 0 Z" fill="none" stroke="#ced9cc" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_tlzbys =
    '<svg viewBox="0.0 0.0 49.9 50.9" ><path transform="translate(0.0, 0.0)" d="M 21 0 L 28.87411117553711 0 C 40.47209167480469 0 49.87411117553711 9.402019500732422 49.87411117553711 21 L 49.87411117553711 29.93230819702148 C 49.87411117553711 41.53028869628906 40.47209167480469 50.93230819702148 28.87411117553711 50.93230819702148 L 21 50.93230819702148 C 9.402019500732422 50.93230819702148 0 41.53028869628906 0 29.93230819702148 L 0 21 C 0 9.402019500732422 9.402019500732422 0 21 0 Z" fill="none" stroke="#ced9cc" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_fphmh4 =
    '<svg viewBox="65.1 0.0 49.9 50.9" ><path transform="translate(65.05, 0.0)" d="M 21 0 L 28.87411117553711 0 C 40.47209167480469 0 49.87411117553711 9.402019500732422 49.87411117553711 21 L 49.87411117553711 29.93230819702148 C 49.87411117553711 41.53028869628906 40.47209167480469 50.93230819702148 28.87411117553711 50.93230819702148 L 21 50.93230819702148 C 9.402019500732422 50.93230819702148 0 41.53028869628906 0 29.93230819702148 L 0 21 C 0 9.402019500732422 9.402019500732422 0 21 0 Z" fill="none" stroke="#ced9cc" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
