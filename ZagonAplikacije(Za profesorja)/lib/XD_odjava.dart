import 'dart:async';

import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:page_transition/page_transition.dart';
import './XD_prijava.dart';
import 'package:adobe_xd/page_link.dart';
import 'package:flutter_svg/flutter_svg.dart';

class XD_odjava extends StatelessWidget {
  XD_odjava({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 2), () {
      Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.fade,
            child: XD_prijava()
        ),
      );
    });
    return Scaffold(
      backgroundColor: const Color(0xffececec),
      body: Stack(
        children: <Widget>[
          Container(),
          Pinned.fromPins(
            Pin(start: 32.0, end: 0.0),
            Pin(size: 196.0, middle: 0.4759),
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: const Color(0xff78b856),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      bottomLeft: Radius.circular(80.0),
                    ),
                  ),
                ),
                Pinned.fromPins(
                  Pin(start: 48.0, end: 2.0),
                  Pin(size: 102.0, end: 28.0),
                  child: Text(
                    'Uspešno ste se odjavili!',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 32,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                      height: 1.25,
                    ),
                    textHeightBehavior:
                        TextHeightBehavior(applyHeightToFirstAscent: false),
                  ),
                ),
                Pinned.fromPins(
                  Pin(startFraction: 0.1395, endFraction: 0.4079),
                  Pin(size: 16.0, middle: 0.2167),
                  child: Text(
                    'Dravske elektrarne Maribor',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 11,
                      color: const Color(0xb2ffffff),
                      letterSpacing: 0.9999999694824219,
                      height: 1.8181818181818181,
                    ),
                    textHeightBehavior:
                        TextHeightBehavior(applyHeightToFirstAscent: false),
                    softWrap: false,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}