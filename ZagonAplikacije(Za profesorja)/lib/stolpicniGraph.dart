/// Bar chart example
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:neki/graphs.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:http/http.dart' as http;

class MyHomePage extends StatefulWidget {

  MyHomePage();


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  late TooltipBehavior _tooltipBehavior;
  List<TabelaMariborskiOtok> chartData1 = [];

  late Future<TabelaMariborskiOtok> futureTabela;

  Future<TabelaMariborskiOtok> fetchTabela() async {
    final response = await http.get(Uri.parse('http://10.0.2.2:5005/Dnevni'));

    var usersData = jsonDecode(response.body);
    List<dynamic> tabelaJSON = List<dynamic>.from(usersData);


    for (int a = 0; a < tabelaJSON.length; a++) {
      chartData1.add(TabelaMariborskiOtok.fromJson(tabelaJSON, a));
      TabelaMariborskiOtok.fromJson(tabelaJSON, a);


    }

    print(chartData1);
    print(chartData1.length);
    return TabelaMariborskiOtok(
        date: "", production: 0, plan: 0);
  }





  @override
  void initState() {
    futureTabela = fetchTabela();
    _tooltipBehavior = TooltipBehavior(enable: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: SfCartesianChart(
            zoomPanBehavior: ZoomPanBehavior(
              enablePanning: true,
            ),
            title: ChartTitle(
                text: 'Mariborski otok produkcija/plan'),
            legend: Legend(isVisible: true),
            tooltipBehavior: _tooltipBehavior,
            series: <ChartSeries>[
              ColumnSeries<TabelaMariborskiOtok, String>(
                color: Color(0xff4095d2),
                  dataSource: chartData1,
                  xValueMapper: (TabelaMariborskiOtok exp, _) => exp.date.toString(),
                  yValueMapper: (TabelaMariborskiOtok exp, _) => (exp.plan*0.001).roundToDouble(),
                  name: 'Plan',
                  markerSettings: MarkerSettings(
                    isVisible: true,
                  )),
              ColumnSeries<TabelaMariborskiOtok, String>(
                color: Color(0xff78b856),
                  dataSource: chartData1,
                  xValueMapper: (TabelaMariborskiOtok exp, _) => exp.date.toString(),
                  yValueMapper: (TabelaMariborskiOtok exp, _) => (exp.production*0.001).roundToDouble(),
                  name: 'Produkcija',
                  markerSettings: MarkerSettings(
                    isVisible: true,
                  )),
            ],
            primaryXAxis: CategoryAxis(
              interval: 1,
              edgeLabelPlacement: EdgeLabelPlacement.shift,
              visibleMaximum: 6,
            ),
          ),
        ));
  }
}

class TabelaMariborskiOtok {
  TabelaMariborskiOtok(
      {required this.date, required this.production, required this.plan});
  final String date;
  final double production;
  final double plan;
  factory TabelaMariborskiOtok.fromJson(List<dynamic> parsedJson, int index) {
    return TabelaMariborskiOtok(
      date: parsedJson[index]['date'].toString(),
      production: double.parse(parsedJson[index]['production__kwh']),
      plan: parsedJson[index]['production_plan__kwh'],
    );
  }
}
