import 'package:flutter/material.dart';
import 'package:neki/XD_graf.dart';
import 'XD_dashboard.dart';
import 'graphs.dart' as Graph;
import 'table.dart' as Tabela;
import 'verification.dart' as Verification;
import 'XD_pozdravljeni.dart';
import 'stolpicniGraph.dart' as Stolpec;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: XD_pozdravljeni(),
      routes: {
        "/Login": (context) => XD_pozdravljeni(),
        "/Navbar": (context) => NavigationExample(),
        //LandingScreen.id: (context) => LandingScreen(),
      },
    );
  }
}

class NavigationExample extends StatefulWidget {
  const NavigationExample({Key? key}) : super(key: key);

  @override
  State<NavigationExample> createState() => _NavigationExampleState();
}

class _NavigationExampleState extends State<NavigationExample> {
  int currentPageIndex = 0;
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: NavigationBarTheme(
        data: NavigationBarThemeData(
          indicatorColor: Color(0xff4095d2),
          backgroundColor: Color(0xff3c4e59),
          labelTextStyle: MaterialStateProperty.all(
            TextStyle(
                fontSize: 14, fontWeight: FontWeight.w400, color: Colors.white),
          ),
        ),
        child: NavigationBar(
          onDestinationSelected: (int index) {
            setState(() {
              currentPageIndex = index;
            });
          },
          selectedIndex: currentPageIndex,
          destinations: const <Widget>[
            NavigationDestination(
              selectedIcon: Icon(Icons.list),
              icon: Icon(Icons.list, color: Colors.white),
              label: 'Tabela',
            ),
            NavigationDestination(
              icon: Icon(Icons.bar_chart, color: Colors.white),
              label: 'Grafi',
            ),
            NavigationDestination(
              icon: Icon(Icons.bar_chart, color: Colors.white),
              label: 'Verifikacija',
            ),
            NavigationDestination(
              icon: Icon(Icons.logout, color: Colors.white),
              label: 'Logout',
            ),
          ],
        ),
      ),
      body: <Widget>[
        Container(
          color: Colors.white,
          alignment: Alignment.center,
          child: Tabela.Tabela(),
        ),
        Container(
          color: Colors.white,
          alignment: Alignment.center,
          child: Graph.Graf(),
        ),
        Container(
          color: Colors.white,
          alignment: Alignment.center,
          child: Verification.Verificatoin(),
        ),
        /*Container(
          color: Colors.white,
          alignment: Alignment.topRight,
          child: Image(
            image: AssetImage('assets/dem_logo.png'),
            width: 200,
            height: 150,
          ),*/
        Container(
          color: Colors.white,
          alignment: Alignment.center,
          child: Stolpec.MyHomePage(),
        ),
      ][currentPageIndex],
    );
  }
}
