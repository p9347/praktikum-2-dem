<br />
<div align="center">
    <a href="https://www.dem.si/">
    <img src="images/demlogo.svg" alt="Logo" width="100" height="100">
  </a>

  <h3 align="center">Mobilna aplikacija za Dravske elektrarne Maribor</h3>

</div>

Mobilna aplikacija je poenostavljena različica Bilten platforme, kjer lahko zaposleni Dravskih elektrarn Maribor dostopajo do podatkov na enostaven način od kjerkoli in kadarkoli.
Uporabili smo Flutter, ki omogoča razvoj aplikacije za več operacijskih sistemov hkrati (Android in iOS).
Podatki so prikazani v tabelah in grafih. Za grafe smo uporabili Googlovo knjižnico za grafe.
Prototipe smo oblikovali v Adobe XD-ju. Nato smo idejo še samo idealizirali v Dartu. Pri tem so barve aplikacije v skladu z njihovo celotno grafično podobo. 
V podjetju DEM je varnost ključnega pomena, zato smo tudi aplikacijo še dodatno zaščitili z dvofaznim overjanjem preko aplikacije Microsoft Authenticator. Do podatkov dostopamo preko API-ja in jih pridobimo v obliki JSON.
Na backendu smo uporabili Express.js, za podatkovno bazo pa MongoDB.

<br/>

<details>
  <summary>Kazalo</summary>
  <ol>
    <li>
      <a href="#navodila-za-namestitev-aplikacije">Navodila za namestitev aplikacije</a>
    </li>
    <li>
      <a href="#tehnološki-sklad">Tehnološki sklad</a>
    </li>
    <li>
      <a href="#avtorji">Avtorji</a>
    </li>
  </ol>
</details>


### Navodila za namestitev aplikacije
1. Potrebujete razvijalno okolje npr. Visual Studio Code, Android Studio, kjer zaženete server.js in main.dart lahko uporabite emulator ali pa fizično napravo (Pazite, da boste na istem omrežju).
2. Za login uporabite uporabniško ime: dem.si/sara, geslo: demgeslo123
3. Včasih je potrebno kliknati še enkrat na datum, da se prikažejo podatki tabele.

<br/>

### Tehnološki sklad
* [Flutter](https://flutter.dev/?gclsrc=ds&gclsrc=ds)
* [Node.js](https://nodejs.org/en/)
* [Express.js](https://expressjs.com/)
* [MongoDB Atlas](https://www.mongodb.com/atlas/database)
* [Adobe XD](https://www.adobe.com/products/xd.html)

<br/>

### Avtorji
* [Sara Petecin](www.linkedin.com/in/sara-petecin)
* [Martina Tivadar](www.linkedin.com/in/martina-tivadar)
* [Vito Polenek](https://www.linkedin.com/in/vito-polenek-058312202/)
* [Rok Ošlovnik](www.linkedin.com/in/rok-oslovnik)