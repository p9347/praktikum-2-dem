import 'package:flutter/material.dart';

class tabelaLetno extends StatefulWidget {
  @override
  _tabelaLetno createState() => _tabelaLetno();
}

class _tabelaLetno extends State<tabelaLetno> {
  @override
  Widget build(BuildContext context) {
    return DataTable(
              columnSpacing: 0,
              columns: [
                DataColumn(
                    label: Text('Sistemske storitve',
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.bold))),
                DataColumn(
                    label: Text('letno (%)',
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.bold))),
                DataColumn(
                    label: Text('letno (cilj)',
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.bold))),
                DataColumn(
                    label: Text('Št. aktivacij',
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.bold))),
              ],
              rows: [
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('RVF'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('98,8 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text(''))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('aRPF (+)'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('97,3 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text(''))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('aRPF (-)'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('97,1 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text(''))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('aRPF'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('97,2 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text(''))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('prRPF'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('100,0 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('0'))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('nrRPF'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('100,0 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('1'))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('regulacija Q/U'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('100,0 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text(''))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('Zagon iz teme'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('100,0 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('0'))),
                ]),
              ],
            );
  }
}
