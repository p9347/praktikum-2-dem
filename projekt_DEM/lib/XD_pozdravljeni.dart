import 'dart:async';
import 'package:flutter/material.dart';
import 'package:adobe_xd/pinned.dart';
import 'package:transition/transition.dart';
import './XD_prijava.dart';

class XD_pozdravljeni extends StatelessWidget {
  XD_pozdravljeni({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 3), () {
      Navigator.push(
          context,
          Transition(
              child: XD_prijava(), transitionEffect: TransitionEffect.FADE));    });
    return Scaffold(
      backgroundColor: const Color(0xffececec),
      body: Stack(
        children: <Widget>[
          Container(),
          Container(
            color: const Color(0xff000000),
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 124.0),
          ),
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: const AssetImage('assets/images/dem_elek.jpg'),
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.83), BlendMode.dstIn),
              ),
            ),
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 124.0),
          ),
          Pinned.fromPins(
            Pin(start: 32.0, end: 0.0),
            Pin(size: 196.0, end: 49.0),
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: const Color(0xff78b856),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      bottomLeft: Radius.circular(80.0),
                    ),
                  ),
                ),
                Pinned.fromPins(
                  Pin(start: 48.0, end: 2.0),
                  Pin(size: 78.0, middle: 0.5593),
                  child: SingleChildScrollView(
                    primary: false,
                    child: Text(
                      'Pozdravljeni!\n',
                      style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 32,
                        color: const Color(0xffffffff),
                        fontWeight: FontWeight.w700,
                        height: 1.25,
                      ),
                      textHeightBehavior:
                          TextHeightBehavior(applyHeightToFirstAscent: false),
                    ),
                  ),
                ),
                Pinned.fromPins(
                  Pin(startFraction: 0.1395, endFraction: 0.4079),
                  Pin(size: 16.0, middle: 0.2167),
                  child: Text(
                    'Dravske elektrarne Maribor',
                    style: TextStyle(
                      fontFamily: 'Poppins',
                      fontSize: 11,
                      color: const Color(0xb2ffffff),
                      letterSpacing: 0.9999999694824219,
                      height: 1.8181818181818181,
                    ),
                    textHeightBehavior:
                        TextHeightBehavior(applyHeightToFirstAscent: false),
                    softWrap: false,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
