import 'package:flutter/material.dart';
import 'package:neki/XD_graf.dart';
import 'package:neki/XD_odjava.dart';
import 'XD_dashboard.dart';
import 'XD_pozdravljeni.dart';
import 'XD_dnevno_porocilo.dart';

void main() {
  runApp(const DEM_app());
}

class DEM_app extends StatelessWidget {
  const DEM_app({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: XD_pozdravljeni(),
      routes: {
        "/Login": (context) => XD_pozdravljeni(),
        "/Navbar": (context) => NavigationExample(),
        //LandingScreen.id: (context) => LandingScreen(),
      },
    );
  }
}

class NavigationExample extends StatefulWidget {
  const NavigationExample({Key? key}) : super(key: key);

  @override
  State<NavigationExample> createState() => _NavigationExampleState();
}

class _NavigationExampleState extends State<NavigationExample> {
  int currentPageIndex = 0;
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: NavigationBarTheme(
        data: NavigationBarThemeData(
          indicatorColor: Color(0x80e4e4e4),
          backgroundColor: Color(0xff78b856),
          labelTextStyle: MaterialStateProperty.all(
            TextStyle(
                fontSize: 12, fontWeight: FontWeight.w400, color: Colors.white),
          ),
        ),
        child: NavigationBar(
          onDestinationSelected: (int index) {
            setState(() {
              currentPageIndex = index;
            });
          },
          selectedIndex: currentPageIndex,
          destinations: const <Widget>[
            NavigationDestination(
              icon: Icon(Icons.list_outlined, color: Colors.white),
              label: 'Elektrarne',
            ),
            NavigationDestination(
              icon: Icon(Icons.info, color: Colors.white),
              label: 'Dnevno Poročilo',


            ),
            NavigationDestination(
              icon: Icon(Icons.show_chart, color: Colors.white),
              label: 'Graf DEM',
            ),
            NavigationDestination(
              icon: Icon(Icons.logout, color: Colors.white),
              label: 'Logout',
            ),
          ],
        ),
      ),
      body: <Widget>[
        Container(
          color: Colors.white,
          alignment: Alignment.center,
          child: XD_dashboard(),
        ),
        Container(
          color: Colors.white,
          alignment: Alignment.center,
          child: XD_dnevno_porocilo(),
        ),
        Container(
          color: Colors.white,
          alignment: Alignment.center,
          child: XD_graf(),
        ),
        Container(
          color: Colors.white,
          alignment: Alignment.center,
          child: XD_odjava(),
        ),
      ][currentPageIndex],
    );
  }
}
