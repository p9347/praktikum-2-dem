import 'dart:convert';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:neki/table.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:http/http.dart' as http;
import 'dart:developer';

class IzrisTabele extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  IzrisTabele();

  @override
  _IzrisTabele createState() => _IzrisTabele();
}

class _IzrisTabele extends State<IzrisTabele> {
  Future<Tabela> fetchTabela() async {
    final response = await http.get(Uri.parse('http://studentdocker.informatika.uni-mb.si:5005/Letni'));

    var usersData = jsonDecode(response.body);
    List<dynamic> a = List<dynamic>.from(usersData);


    return Tabela.fromJson(a);
  }

  late Future<Tabela> futureTabela;

  @override
  void initState() {
    super.initState();
    futureTabela = fetchTabela();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FutureBuilder<Tabela>(
          future: futureTabela,
          builder: (context, snapshot) {

            if (snapshot.hasData) {
              return Table(
                border: TableBorder.all(),
                columnWidths: const <int, TableColumnWidth>{
                },
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                children: <TableRow>[
                  TableRow(
                    children: <Widget>[
                      Text(
                        "DEM",
                        textAlign: TextAlign.center,
                      ),
                      TableCell(
                        child: Text(
                          "Produkcija",
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Text(
                        "Plan",
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                  TableRow(
                    children: <Widget>[
                      Text(
                        "Letna",
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        snapshot.data!.produkcija.toString(),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        snapshot.data!.produkcijski_plan.toString(),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }

            // By default, show a loading spinner.
            return const CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}


class Tabela {
  final num produkcija;
  final num produkcijski_plan;

  const Tabela({
    required this.produkcija,
    required this.produkcijski_plan,
  });

  factory Tabela.fromJson(List<dynamic> json) {
    return Tabela(
      produkcija: json[0]['joint_dem']['production__kwh'],
      produkcijski_plan: json[0]['joint_dem']['production_plan__kwh'],
    );
  }
}
