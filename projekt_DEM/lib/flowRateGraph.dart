import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:neki/table.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'tabela.dart' as Tabela;
import 'package:http/http.dart' as http;

class Graf extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  Graf();

  @override
  _GrafState createState() => _GrafState();
}

class _GrafState extends State<Graf> {
  late TooltipBehavior _tooltipBehavior;
  List<FlowRate> chartData = [];

  late Future<FlowRate> futureTabela;

  Future<FlowRate> fetchTabela() async {
    final response = await http.get(Uri.parse('http://studentdocker.informatika.uni-mb.si:5005/flowRate'));
    final neki = await http.get(Uri.parse('http://studentdocker.informatika.uni-mb.si:5005/flowRatePlan'));


    var plan =jsonDecode(neki.body);
    var usersData = jsonDecode(response.body);
    List<dynamic> tabelaJSON = List<dynamic>.from(usersData);
    List<dynamic> planJSON = List<dynamic>.from(plan);
    //tabelaJSON.add(plan["flow_rate"]);


    for (int i = 0; i < tabelaJSON.length; i++) {

      chartData.add(FlowRate.fromJson(tabelaJSON, i, planJSON[i]['flow_rate']));

      FlowRate.fromJson(tabelaJSON, i, planJSON[i]['flow_rate']);

    }



    return FlowRate(
        date: "month", production: 10000000, plan: 5000000000000);
  }

  @override
  void initState() {
    super.initState();
    _tooltipBehavior = TooltipBehavior(enable: true);
    futureTabela = fetchTabela();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: FutureBuilder(
                future: futureTabela,
                builder: (context, snapshot) {
                    return SfCartesianChart(
                        legend: Legend(isVisible: true),
                        tooltipBehavior: _tooltipBehavior,
                        zoomPanBehavior: ZoomPanBehavior(
                          enablePinching: true,
                          zoomMode: ZoomMode.x,
                          enablePanning: true,
                        ),
                        primaryXAxis: CategoryAxis(
                          interval: 1,
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          visibleMaximum: 6,
                        ),
                        // Chart title
                        series: <ChartSeries<FlowRate, String>>[
                          LineSeries<FlowRate, String>(
                            color: Color(0xff78b856),
                            dataSource: chartData,
                            xValueMapper: (FlowRate date, _) =>
                                date.date.toString(),
                            yValueMapper:
                                (FlowRate production, _) =>
                                production.production.toDouble(),
                            name: 'Pretok',
                            markerSettings: MarkerSettings(
                              isVisible: true,
                            ),
                          ),
                          LineSeries<FlowRate, String>(
                            color: Color(0xff4095d2),
                            dataSource: chartData,
                            dashArray: <double>[3, 3],
                            xValueMapper: (FlowRate date, _) =>
                                date.date.toString(),
                            yValueMapper: (FlowRate plan, _) =>
                                plan.plan.toDouble(),
                            name: 'Plan',
                            markerSettings: MarkerSettings(
                              isVisible: true,
                            ),
                          ),
                        ]);

                  return Card();
                })));
  }
}

class FlowRate {
  FlowRate(
      {required this.date, required this.production, required this.plan});
  final String date;
  final double production;
  final double plan;
  factory FlowRate.fromJson(List<dynamic> parsedJson, int index, int asd) {
    return FlowRate(
      date: parsedJson[index]['date'].toString(),
      production: parsedJson[index]['flow_rate'],
      plan: asd.toDouble(),
    );
  }
}
