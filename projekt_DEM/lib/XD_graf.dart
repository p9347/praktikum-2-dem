import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:http/http.dart' as http;
import 'package:adobe_xd/pinned.dart';
import 'package:page_transition/page_transition.dart';
import 'package:toggle_switch/toggle_switch.dart';

import 'XD_tabela.dart';

class XD_graf extends StatefulWidget {
  XD_graf();

  @override
  _XD_graf createState() => _XD_graf();
}

class _XD_graf extends State<XD_graf> {
  late TooltipBehavior _tooltipBehavior;
  List<TabelaMariborskiOtok> chartData = [];
  int? stevilo = 0;
  bool videnLine = true;
  bool videnBar = false;
  bool vidnaTabela = false;

  void sortiraj(int? index) {
    if (index == 0) {

      setState(() {
        this.videnLine = true;
        this.videnBar = false;
        this.vidnaTabela = false;
        stevilo = 0;
      });
    } else if (index == 1) {
      setState(() {
        this.videnLine = false;
        this.videnBar = true;
        this.vidnaTabela = false;
        stevilo = 1;
      });
    } else if (index == 2) {
      setState(() {
        this.videnLine = false;
        this.videnBar = false;
        this.vidnaTabela = true;
        stevilo = 2;
      });
    }
  }



  late Future<TabelaMariborskiOtok> futureTabela;

  Future<TabelaMariborskiOtok> fetchTabela() async {
    final response = await http.get(
        Uri.parse('http://studentdocker.informatika.uni-mb.si:5005/Dnevni'));

    var usersData = jsonDecode(response.body);
    List<dynamic> tabelaJSON = List<dynamic>.from(usersData);


    for (int i = 0; i < tabelaJSON.length; i++) {
      chartData.add(TabelaMariborskiOtok.fromJson(tabelaJSON, i));

      TabelaMariborskiOtok.fromJson(tabelaJSON, i);
    }

    return TabelaMariborskiOtok(
        date: "month", production: 10000000, plan: 5000000000000);
  }

  @override
  void initState() {
    super.initState();
    _tooltipBehavior = TooltipBehavior(enable: true);
    futureTabela = fetchTabela();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xffececec),
        appBar: AppBar(
          backgroundColor: const Color(0xff78b856),
          automaticallyImplyLeading: false,
          title: Text(
            'Proizvodnja DEM',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'Poppins',
              fontSize: 20,
              color: Colors.white,
              letterSpacing: 0.4,
              fontWeight: FontWeight.w700,
            ),
            softWrap: false,
          ),
        ),
        body: ListView(children: <Widget>[
          Center(
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: ToggleSwitch(
                minWidth: 90.0,
                minHeight: 40.0,
                fontSize: 16.0,
                initialLabelIndex: stevilo,
                activeBgColor: [Color(0xff78b856)],
                activeFgColor: Colors.white,
                inactiveBgColor: Colors.white,
                inactiveFgColor: Colors.grey,
                totalSwitches: 3,
                icons: [
                  Icons.show_chart,
                  Icons.bar_chart,
                  Icons.table_chart,
                ],
                onToggle: (index) {
                  sortiraj(index);
                },
              ),
            ),
          ),
          Visibility(
          visible: videnLine,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment(0.005, -0.037),
                child: SizedBox(
                    width: 380.0,
                    height: 480.0,
                    child: FutureBuilder(
                        future: futureTabela,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return SfCartesianChart(
                                legend: Legend(isVisible: true),
                                tooltipBehavior: _tooltipBehavior,
                                zoomPanBehavior: ZoomPanBehavior(
                                  enablePinching: true,
                                  zoomMode: ZoomMode.x,
                                  enablePanning: true,
                                ),
                                primaryXAxis: CategoryAxis(
                                  interval: 1,
                                  edgeLabelPlacement: EdgeLabelPlacement.shift,
                                ),
                                // Chart title
                                //title: ChartTitle(text: 'Produkcijski plan'),
                                series: <
                                    ChartSeries<TabelaMariborskiOtok, String>>[
                                  LineSeries<TabelaMariborskiOtok, String>(
                                    color: Color(0xff78b856),
                                    dataSource: chartData,
                                    xValueMapper:
                                        (TabelaMariborskiOtok date, _) =>
                                            date.date.toString(),
                                    yValueMapper:
                                        (TabelaMariborskiOtok production, _) =>
                                            (production.production * 0.001)
                                                .roundToDouble(),
                                    name: 'Produkcija',
                                    markerSettings: MarkerSettings(
                                      isVisible: false,
                                    ),
                                  ),
                                  LineSeries<TabelaMariborskiOtok, String>(
                                    color: Color(0xff4095d2),
                                    dataSource: chartData,
                                    dashArray: <double>[3, 3],
                                    xValueMapper:
                                        (TabelaMariborskiOtok date, _) =>
                                            date.date.toString(),
                                    yValueMapper:
                                        (TabelaMariborskiOtok plan, _) =>
                                            (plan.plan * 0.001).roundToDouble(),
                                    name: 'Plan',
                                    markerSettings: MarkerSettings(
                                      isVisible: false,
                                    ),
                                  ),
                                ]);
                          }
                          return Card();
                        })),
              ),
            ],
          ),),
          Visibility(
            visible: videnBar,
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment(0.005, -0.037),
                  child: SizedBox(
                      width: 380.0,
                      height: 480.0,
                      child: FutureBuilder(
                          future: futureTabela,
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return SfCartesianChart(
                                  legend: Legend(isVisible: true),
                                  tooltipBehavior: _tooltipBehavior,
                                  zoomPanBehavior: ZoomPanBehavior(
                                    enablePinching: true,
                                    zoomMode: ZoomMode.x,
                                    enablePanning: true,
                                  ),
                                  primaryXAxis: CategoryAxis(
                                    interval: 1,
                                    edgeLabelPlacement: EdgeLabelPlacement.shift,
                                    visibleMaximum: 30,
                                  ),
                                  // Chart title
                                  //title: ChartTitle(text: 'Produkcijski plan'),
                                  series: <
                                      ChartSeries<TabelaMariborskiOtok, String>>[
                                    ColumnSeries<TabelaMariborskiOtok, String>(
                                      color: Color(0xff78b856),
                                      dataSource: chartData,
                                      xValueMapper:
                                          (TabelaMariborskiOtok date, _) =>
                                          date.date.toString(),
                                      yValueMapper:
                                          (TabelaMariborskiOtok production, _) =>
                                          (production.production * 0.001)
                                              .roundToDouble(),
                                      name: 'Produkcija',
                                      markerSettings: MarkerSettings(
                                        isVisible: false,
                                      ),
                                    ),
                                    ColumnSeries<TabelaMariborskiOtok, String>(
                                      color: Color(0xff4095d2),
                                      dataSource: chartData,
                                      dashArray: <double>[3, 3],
                                      xValueMapper:
                                          (TabelaMariborskiOtok date, _) =>
                                          date.date.toString(),
                                      yValueMapper:
                                          (TabelaMariborskiOtok plan, _) =>
                                          (plan.plan * 0.001).roundToDouble(),
                                      name: 'Plan',
                                      markerSettings: MarkerSettings(
                                        isVisible: false,
                                      ),
                                    ),
                                  ]);
                            }
                            return Card();
                          })),
                ),
              ],
            ),),Visibility(
            visible: vidnaTabela,
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment(0.005, -0.037),
                  child: SizedBox(
                      width: 380.0,
                      height: 480.0,
                      child: XD_tabela()
                  ),
                ),
              ],
            ),),
        ]));

  }
}

class TabelaMariborskiOtok {
  TabelaMariborskiOtok(
      {required this.date, required this.production, required this.plan});

  final String date;
  final double production;
  final double plan;

  factory TabelaMariborskiOtok.fromJson(List<dynamic> parsedJson, int index) {
    return TabelaMariborskiOtok(
      date: parsedJson[index]['date'].toString(),
      production: double.parse(parsedJson[index]['production__kwh']),
      plan: parsedJson[index]['production_plan__kwh'],
    );
  }
}

const String _svg_yly9z6 =
    '<svg viewBox="0.0 0.0 412.0 90.0" ><path  d="M 0 0 L 412 0 L 412 90 L 0 90 L 0 0 Z" fill="#78b856" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_qfpsq3 =
    '<svg viewBox="21.0 30.0 20.0 30.0" ><path transform="translate(8.25, 26.0)" d="M 30.29386138916016 34 L 12.75 19.00000190734863 L 30.29386138916016 4 L 32.75 6.137499809265137 L 17.70614242553711 19.00000190734863 L 32.75 31.86250305175781 L 30.29386138916016 34 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
const String _svg_nuufo =
    '<svg viewBox="360.0 30.0 30.0 30.0" ><path transform="translate(354.0, 24.0)" d="M 16.625 19.75 L 31.125 19.75 L 26.87499809265137 15.49999904632568 L 28.66666603088379 13.70833301544189 L 36 21.04166603088379 L 28.74999809265137 28.29166603088379 L 26.95833206176758 26.5 L 31.20833206176758 22.25 L 16.625 22.25 L 16.625 19.75 Z M 20.62499809265137 6 L 20.62499809265137 8.5 L 8.5 8.5 C 8.5 8.5 8.5 8.5 8.5 8.5 C 8.5 8.5 8.5 8.5 8.5 8.5 L 8.5 33.5 C 8.5 33.5 8.5 33.5 8.5 33.5 C 8.5 33.5 8.5 33.5 8.5 33.5 L 20.62499809265137 33.5 L 20.62499809265137 36 L 8.5 36 C 7.833333015441895 36 7.25 35.75 6.75 35.24999618530273 C 6.25 34.75 6 34.16666412353516 6 33.5 L 6 8.5 C 6 7.833333015441895 6.25 7.25 6.75 6.75 C 7.25 6.25 7.833333015441895 6 8.5 6 L 20.62499809265137 6 Z" fill="#ffffff" stroke="none" stroke-width="1" stroke-miterlimit="4" stroke-linecap="butt" /></svg>';
