import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'dart:async';
import 'main.dart' as Navbar;
import 'package:page_transition/page_transition.dart';

class XD_verifikacija extends StatefulWidget {
  const XD_verifikacija({Key? key}) : super(key: key);

  @override
  _XD_verifikacija createState() => _XD_verifikacija();
}

class _XD_verifikacija extends State<XD_verifikacija> {
  bool _isResendAgain = false;
  bool _isVerified = false;
  bool _isLoading = false;

  String _code = '';

  late Timer _timer;
  int _start = 30;
  int _currentIndex = 0;

  void resend() {
    setState(() {
      _isResendAgain = true;
    });

    const oneSec = Duration(seconds: 1);
    _timer = new Timer.periodic(oneSec, (timer) {
      setState(() {
        if (_start == 0) {
          _start = 30;
          _isResendAgain = false;
          timer.cancel();
        } else {
          _start--;
        }
      });
    });
  }

  verify() {
    setState(() {
      _isLoading = true;
    });

    const oneSec = Duration(milliseconds: 2000);
    _timer = new Timer.periodic(oneSec, (timer) {
      setState(() {
        _isLoading = false;
        _isVerified = true;
      });
    });
  }

  @override
  void initState() {
    Timer.periodic(Duration(seconds: 5), (timer) {
      setState(() {
        _currentIndex++;

        if (_currentIndex == 3) _currentIndex = 0;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
              height: MediaQuery.of(context).size.height,
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 100,
                    child: Stack(children: [
                      Positioned(
                        top: 0,
                        left: 50,
                        right: 50,
                        bottom: 0,
                        child: AnimatedOpacity(
                          opacity: _currentIndex == 0 ? 1 : 0,
                          duration: Duration(
                            seconds: 1,
                          ),
                          curve: Curves.linear,
                          child:
                              Image(image: AssetImage('assets/dem_logo.png')),
                        ),
                      ),
                      Positioned(
                        top: 0,
                        left: 50,
                        right: 50,
                        bottom: 0,
                        child: AnimatedOpacity(
                          opacity: _currentIndex == 1 ? 1 : 0,
                          duration: Duration(seconds: 1),
                          curve: Curves.linear,
                          child:
                              Image(image: AssetImage('assets/dem_logo.png')),
                        ),
                      ),
                      Positioned(
                        top: 0,
                        left: 50,
                        right: 50,
                        bottom: 0,
                        child: AnimatedOpacity(
                          opacity: _currentIndex == 2 ? 1 : 0,
                          duration: Duration(seconds: 1),
                          curve: Curves.linear,
                          child:
                              Image(image: AssetImage('assets/dem_logo.png')),
                        ),
                      )
                    ]),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  FadeInDown(
                    delay: Duration(milliseconds: 500),
                    duration: Duration(milliseconds: 500),
                    child: Text(
                      "Vnesite verifikacijsko 6 mestno kodo poslano \n v Microsoft Authenticator",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.grey.shade500,
                          height: 1.5),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  // Verification Code Input
                  FadeInDown(
                    delay: Duration(milliseconds: 600),
                    duration: Duration(milliseconds: 500),
                    child: OtpTextField(
                      numberOfFields: 6,
                      borderColor: const Color(0xff78b856),
                      focusedBorderColor: const Color(0xff78b856),
                      showFieldAsBox: false,
                      borderWidth: 4.0,
                      //runs when a code is typed in
                      onCodeChanged: (String code) {
                        //handle validation or checks here if necessary
                      },
                      //runs when every textfield is filled
                      onSubmit: (String verificationCode) {},
                    ),
                  ),

                  SizedBox(
                    height: 0,
                  ),
                  FadeInDown(
                    delay: Duration(milliseconds: 700),
                    duration: Duration(milliseconds: 500),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Niste prejeli kode?",
                          style: TextStyle(
                              fontSize: 14, color: Colors.grey.shade500),
                        ),
                        TextButton(
                            onPressed: () {
                              if (_isResendAgain) return;
                              resend();
                            },
                            child: Text(
                              _isResendAgain
                                  ? "Poskusite še enkrat v " + _start.toString()
                                  : "Ponovno pošljite",
                              style: TextStyle(color: Color(0xff3c4e59)),
                            ))
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  FadeInDown(
                    delay: Duration(milliseconds: 800),
                    duration: Duration(milliseconds: 500),
                    child: MaterialButton(
                      elevation: 0,
                      onPressed: _code.length < 7
                          ? () => {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        type: PageTransitionType.fade,
                                        child: Navbar.NavigationExample()))
                              }
                          : () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      type: PageTransitionType.fade,
                                      child: Navbar.NavigationExample()));
                            },
                      color: const Color(0xff78b856),
                      hoverColor: Color(0xff3c4e59),
                      height: 45,
                      minWidth: MediaQuery.of(context).size.width * 0.8,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(38.0)),
                      child: _isLoading
                          ? Container(
                              width: 20,
                              height: 20,
                              child: CircularProgressIndicator(
                                backgroundColor: Colors.white,
                                strokeWidth: 3,
                                color: Colors.black,
                              ),
                            )
                          : _isVerified
                              ? Icon(
                                  Icons.check_circle,
                                  color: Colors.white,
                                  size: 30,
                                )
                              : Text(
                                  "POTRDI",
                                  style:
                                      TextStyle(color: const Color(0xffffffff)),
                                ),
                    ),
                  )
                ],
              )),
        ));
  }
}
