import 'package:flutter/material.dart';
import 'package:neki/XD_stolpicni_graf.dart';
import 'package:page_transition/page_transition.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class XD_dashboard extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  XD_dashboard();

  @override
  _XD_dashboard createState() => _XD_dashboard();
}

class _XD_dashboard extends State<XD_dashboard> {
  List<NoviFlows> data = [];
  late Future<NoviFlows> futureTabela;

  Future<NoviFlows> fetchTabela() async {
    final response = await http.get(Uri.parse('http://studentdocker.informatika.uni-mb.si:5005/NoviFlow'));

    var usersData = jsonDecode(response.body);
    List<dynamic> tabelaJSON = List<dynamic>.from(usersData);


    for (int i = 0; i < tabelaJSON.length; i++) {
      data.add(NoviFlows.fromJson(tabelaJSON, i));

      NoviFlows.fromJson(tabelaJSON, i);
    }

    return NoviFlows(pretok: 0, moc: 0);
  }

  @override
  void initState() {
    super.initState();
    futureTabela = fetchTabela();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xffececec),
      appBar: AppBar(
        backgroundColor: const Color(0xff78b856),
        automaticallyImplyLeading: false,
        title: Text(
          'Proizvodnja in pretoki',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'Poppins',
            fontSize: 20,
            color: Colors.white,
            letterSpacing: 0.4,
            fontWeight: FontWeight.w700,
          ),
          softWrap: false,
        ),
      ),
      body: FutureBuilder<NoviFlows>(
          future: futureTabela,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: [
                  Card(
                      margin: EdgeInsets.all(20),
                      child: Column(children: [
                        Row(children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Text(
                                  'HE Dravograd',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 15,
                                    color: const Color(0xff707070),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text(
                                  'Moč (MW)',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.24,
                                  ),
                                  softWrap: false,
                                ),
                                Text(
                                  data[0].moc.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text.rich(
                                  TextSpan(
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      color: const Color(0xff707070),
                                      letterSpacing: 0.24,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'Pretok (m',
                                      ),
                                      TextSpan(
                                        text: '3',
                                      ),
                                      TextSpan(
                                        text: '/s)',
                                      ),
                                    ],
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  softWrap: false,
                                ),
                                Text(
                                  data[0].pretok.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Column(children: [
                                Ink(
                                  decoration: const ShapeDecoration(
                                    color: const Color(0xff78b856),
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.show_chart),
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: XD_stolpicni_graf()));
                                    },
                                  ),
                                ),
                              ])),
                        ]),
                      ])),
                  Card(
                      margin: EdgeInsets.all(20),
                      child: Column(children: [
                        Row(children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Text(
                                  'HE Vuzenica',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 15,
                                    color: const Color(0xff707070),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text(
                                  'Moč (MW)',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.24,
                                  ),
                                  softWrap: false,
                                ),
                                Text(
                                  data[1].moc.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text.rich(
                                  TextSpan(
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      color: const Color(0xff707070),
                                      letterSpacing: 0.24,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'Pretok (m',
                                      ),
                                      TextSpan(
                                        text: '3',
                                      ),
                                      TextSpan(
                                        text: '/s)',
                                      ),
                                    ],
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  softWrap: false,
                                ),
                                Text(
                                  data[1].pretok.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Column(children: [
                                Ink(
                                  decoration: const ShapeDecoration(
                                    color: const Color(0xff78b856),
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.show_chart),
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: XD_stolpicni_graf()));
                                    },
                                  ),
                                ),
                              ])),
                        ]),
                      ])),
                  Card(
                      margin: EdgeInsets.all(20),
                      child: Column(children: [
                        Row(children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Text(
                                  'HE Vuhred',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 15,
                                    color: const Color(0xff707070),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text(
                                  'Moč (MW)',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.24,
                                  ),
                                  softWrap: false,
                                ),
                                Text(
                                  data[2].moc.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text.rich(
                                  TextSpan(
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      color: const Color(0xff707070),
                                      letterSpacing: 0.24,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'Pretok (m',
                                      ),
                                      TextSpan(
                                        text: '3',
                                      ),
                                      TextSpan(
                                        text: '/s)',
                                      ),
                                    ],
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  softWrap: false,
                                ),
                                Text(
                                  data[2].pretok.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Column(children: [
                                Ink(
                                  decoration: const ShapeDecoration(
                                    color: const Color(0xff78b856),
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.show_chart),
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: XD_stolpicni_graf()));
                                    },
                                  ),
                                ),
                              ])),
                        ]),
                      ])),
                  Card(
                      margin: EdgeInsets.all(20),
                      child: Column(children: [
                        Row(children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Text(
                                  'HE Ožbalt',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 15,
                                    color: const Color(0xff707070),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text(
                                  'Moč (MW)',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.24,
                                  ),
                                  softWrap: false,
                                ),
                                Text(
                                  data[3].moc.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text.rich(
                                  TextSpan(
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      color: const Color(0xff707070),
                                      letterSpacing: 0.24,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'Pretok (m',
                                      ),
                                      TextSpan(
                                        text: '3',
                                      ),
                                      TextSpan(
                                        text: '/s)',
                                      ),
                                    ],
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  softWrap: false,
                                ),
                                Text(
                                  data[3].pretok.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Column(children: [
                                Ink(
                                  decoration: const ShapeDecoration(
                                    color: const Color(0xff78b856),
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.show_chart),
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: XD_stolpicni_graf()));
                                    },
                                  ),
                                ),
                              ])),
                        ]),
                      ])),
                  Card(
                      margin: EdgeInsets.all(20),
                      child: Column(children: [
                        Row(children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Text(
                                  'HE Fala',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 15,
                                    color: const Color(0xff707070),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text(
                                  'Moč (MW)',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.24,
                                  ),
                                  softWrap: false,
                                ),
                                Text(
                                  data[4].moc.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text.rich(
                                  TextSpan(
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      color: const Color(0xff707070),
                                      letterSpacing: 0.24,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'Pretok (m',
                                      ),
                                      TextSpan(
                                        text: '3',
                                      ),
                                      TextSpan(
                                        text: '/s)',
                                      ),
                                    ],
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  softWrap: false,
                                ),
                                Text(
                                  data[4].pretok.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Column(children: [
                                Ink(
                                  decoration: const ShapeDecoration(
                                    color: const Color(0xff78b856),
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.show_chart),
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: XD_stolpicni_graf()));
                                    },
                                  ),
                                ),
                              ])),
                        ]),
                      ])),
                  Card(
                      margin: EdgeInsets.all(20),
                      child: Column(children: [
                        Row(children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Text(
                                  'HE Mariborski otok',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 15,
                                    color: const Color(0xff707070),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text(
                                  'Moč (MW)',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.24,
                                  ),
                                  softWrap: false,
                                ),
                                Text(
                                  data[5].moc.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text.rich(
                                  TextSpan(
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      color: const Color(0xff707070),
                                      letterSpacing: 0.24,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'Pretok (m',
                                      ),
                                      TextSpan(
                                        text: '3',
                                      ),
                                      TextSpan(
                                        text: '/s)',
                                      ),
                                    ],
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  softWrap: false,
                                ),
                                Text(
                                  data[5].pretok.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Column(children: [
                                Ink(
                                  decoration: const ShapeDecoration(
                                    color: const Color(0xff78b856),
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.show_chart),
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: XD_stolpicni_graf()));
                                    },
                                  ),
                                ),
                              ])),
                        ]),
                      ])),
                  Card(
                      margin: EdgeInsets.all(20),
                      child: Column(children: [
                        Row(children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Text(
                                  'HE Zlatoličje',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 15,
                                    color: const Color(0xff707070),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text(
                                  'Moč (MW)',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.24,
                                  ),
                                  softWrap: false,
                                ),
                                Text(
                                  data[6].moc.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text.rich(
                                  TextSpan(
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      color: const Color(0xff707070),
                                      letterSpacing: 0.24,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'Pretok (m',
                                      ),
                                      TextSpan(
                                        text: '3',
                                      ),
                                      TextSpan(
                                        text: '/s)',
                                      ),
                                    ],
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  softWrap: false,
                                ),
                                Text(
                                  data[6].pretok.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Column(children: [
                                Ink(
                                  decoration: const ShapeDecoration(
                                    color: const Color(0xff78b856),
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.show_chart),
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: XD_stolpicni_graf()));
                                    },
                                  ),
                                ),
                              ])),
                        ]),
                      ])),
                  Card(
                      margin: EdgeInsets.all(20),
                      child: Column(children: [
                        Row(children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Text(
                                  'HE Formin',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 15,
                                    color: const Color(0xff707070),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text(
                                  'Moč (MW)',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.24,
                                  ),
                                  softWrap: false,
                                ),
                                Text(
                                  data[7].moc.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text.rich(
                                  TextSpan(
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      color: const Color(0xff707070),
                                      letterSpacing: 0.24,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'Pretok (m',
                                      ),
                                      TextSpan(
                                        text: '3',
                                      ),
                                      TextSpan(
                                        text: '/s)',
                                      ),
                                    ],
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  softWrap: false,
                                ),
                                Text(
                                  data[7].pretok.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Column(children: [
                                Ink(
                                  decoration: const ShapeDecoration(
                                    color: const Color(0xff78b856),
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.show_chart),
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: XD_stolpicni_graf()));
                                    },
                                  ),
                                ),
                              ])),
                        ]),
                      ])),
                  Card(
                      margin: EdgeInsets.all(20),
                      child: Column(children: [
                        Row(children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Text(
                                  'MHE skupaj',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 15,
                                    color: const Color(0xff707070),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text(
                                  'Moč (MW)',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.24,
                                  ),
                                  softWrap: false,
                                ),
                                Text(
                                  data[8].moc.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text.rich(
                                  TextSpan(
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      color: const Color(0xff707070),
                                      letterSpacing: 0.24,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'Pretok (m',
                                      ),
                                      TextSpan(
                                        text: '3',
                                      ),
                                      TextSpan(
                                        text: '/s)',
                                      ),
                                    ],
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  softWrap: false,
                                ),
                                Text(
                                  data[8].pretok.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Column(children: [
                                Ink(
                                  decoration: const ShapeDecoration(
                                    color: const Color(0xff78b856),
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.show_chart),
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: XD_stolpicni_graf()));
                                    },
                                  ),
                                ),
                              ])),
                        ]),
                      ])),
                  Card(
                      margin: EdgeInsets.all(20),
                      child: Column(children: [
                        Row(children: [
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 20, 0, 0),
                                child: Text(
                                  'Sončna',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 15,
                                    color: const Color(0xff707070),
                                    fontWeight: FontWeight.w700,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ]),
                        Row(children: [
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text(
                                  'Moč (MW)',
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 12,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.24,
                                  ),
                                  softWrap: false,
                                ),
                                Text(
                                  data[9].moc.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(28.0),
                              child: Column(children: [
                                Text.rich(
                                  TextSpan(
                                    style: TextStyle(
                                      fontFamily: 'Poppins',
                                      fontSize: 12,
                                      color: const Color(0xff707070),
                                      letterSpacing: 0.24,
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'Pretok (m',
                                      ),
                                      TextSpan(
                                        text: '3',
                                      ),
                                      TextSpan(
                                        text: '/s)',
                                      ),
                                    ],
                                  ),
                                  textHeightBehavior: TextHeightBehavior(
                                      applyHeightToFirstAscent: false),
                                  softWrap: false,
                                ),
                                Text(
                                  data[9].pretok.toString(),
                                  style: TextStyle(
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    color: const Color(0xff707070),
                                    letterSpacing: 0.4,
                                  ),
                                  softWrap: false,
                                ),
                              ])),
                          Padding(
                              padding: EdgeInsets.all(12.0),
                              child: Column(children: [
                                Ink(
                                  decoration: const ShapeDecoration(
                                    color: const Color(0xff78b856),
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Icon(Icons.show_chart),
                                    color: Colors.white,
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          PageTransition(
                                              type: PageTransitionType.fade,
                                              child: XD_stolpicni_graf()));
                                    },
                                  ),
                                ),
                              ])),
                        ]),
                      ])),
                ],
                shrinkWrap: true,
              );
            }
            ;
            return Card();
          }),
    );
  }
}

class NoviFlows {
  NoviFlows({required this.pretok, required this.moc});
  final int pretok;
  final int moc;
  factory NoviFlows.fromJson(List<dynamic> parsedJson, int index) {
    return NoviFlows(
      pretok: parsedJson[index]['flow_rate'],
      moc: parsedJson[index]['power__mw'],
    );
  }
}
