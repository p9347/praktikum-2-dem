import 'package:flutter/material.dart';

class tabelaMesecno extends StatefulWidget {
  @override
  _tabelaMesecno createState() => _tabelaMesecno();
}

class _tabelaMesecno extends State<tabelaMesecno> {
  @override
  Widget build(BuildContext context) {
    return DataTable(
              columnSpacing: 0,
              columns: [
                DataColumn(
                    label: Text('Sistemske storitve',
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.bold))),
                DataColumn(
                    label: Text('mesečno  (%)',
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.bold))),
                DataColumn(
                    label: Text('mesečno (cilj)',
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.bold))),
                DataColumn(
                    label: Text('Št. aktivacij',
                        style: TextStyle(
                            fontSize: 10, fontWeight: FontWeight.bold))),
              ],
              rows: [
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('RVF'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('99,7 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text(''))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('aRPF (+)'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('99,0 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text(''))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('aRPF (-)'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('99,1 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text(''))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('aRPF'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('99,1 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text(''))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('prRPF'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('100,0 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('0'))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('nrRPF'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('100,0 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('0'))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('regulacija Q/U'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('100,0 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text(''))),
                ]),
                DataRow(cells: [
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('Zagon iz teme'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight,
                      child: Text('100,0 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('97 %'))),
                  DataCell(Container(
                      alignment: Alignment.centerRight, child: Text('0'))),
                ]),
              ],
            );
  }
}
