import 'package:adobe_xd/pinned.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:neki/XD_graf.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:http/http.dart' as http;
import 'package:adobe_xd/pinned.dart';
import 'package:page_transition/page_transition.dart';
import 'package:toggle_switch/toggle_switch.dart';
import 'tabelaDnevno.dart' as Dnevno;
import 'tabelaMesecno.dart' as Mesecno;
import 'tabelaLetno.dart' as Letno;

import 'XD_tabela.dart';

class XD_dnevno_porocilo extends StatefulWidget {
  @override
  _XD_dnevno_porocilo createState() => _XD_dnevno_porocilo();
}

class _XD_dnevno_porocilo extends State<XD_dnevno_porocilo> {
  DateTime date = DateTime.now();
  late TooltipBehavior _tooltipBehavior;
  List<TabelaMariborskiOtok> chartData = [];
  bool vidnoDnevno = false;
  bool vidnoMesecno = true;
  bool vidnoLetno = false;
  int? stevio = 1;

  void sortiraj(int? index) {
    if (index == 0) {

      setState(() {
        this.vidnoDnevno = true;
        this.vidnoMesecno = false;
        this.vidnoLetno = false;
        stevio = 0;
      });
    } else if (index == 1) {
      setState(() {
        this.vidnoDnevno = false;
        this.vidnoMesecno = true;
        this.vidnoLetno = false;
        stevio = 1;
      });
    } else if (index == 2) {
      setState(() {
        this.vidnoDnevno = false;
        this.vidnoMesecno = false;
        this.vidnoLetno = true;
        stevio = 2;
      });
    }

  }

  late Future<TabelaMariborskiOtok> futureTabela;

  Future<TabelaMariborskiOtok> fetchTabela() async {
    final response = await http.get(
        Uri.parse('http://studentdocker.informatika.uni-mb.si:5005/Dnevni'));

    var usersData = jsonDecode(response.body);
    List<dynamic> tabelaJSON = List<dynamic>.from(usersData);


    for (int i = 0; i < tabelaJSON.length; i++) {
      chartData.add(TabelaMariborskiOtok.fromJson(tabelaJSON, i));
      TabelaMariborskiOtok.fromJson(tabelaJSON, i);
    }

    return TabelaMariborskiOtok(
        date: "month", production: 10000000, plan: 5000000000000);
  }

  @override
  void initState() {
    super.initState();
    _tooltipBehavior = TooltipBehavior(enable: true);
    futureTabela = fetchTabela();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          backgroundColor: const Color(0xffe4e4e4),
          appBar: AppBar(
            backgroundColor: const Color(0xff78b856),
            automaticallyImplyLeading: false,
            title: Text(
              'Dnevno poročilo',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Poppins',
                fontSize: 20,
                color: Colors.white,
                letterSpacing: 0.4,
                fontWeight: FontWeight.w700,
              ),
              softWrap: false,
            ),
          ),
          body: ListView(
            children: <Widget>[
              Center(
                  child: Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Text(
                        '${date.day}. ${date.month}. ${date.year}',
                        style: TextStyle(
                            fontSize: 20,
                            color: const Color(0xff707070)),
                      ))),
              FloatingActionButton(
                onPressed: () async {
                  DateTime? newDate = await showDatePicker(
                    context: context,
                    initialDate: date,
                    firstDate: DateTime(1900),
                    lastDate: DateTime(2100),
                    builder: (context, child) {
                      return Theme(
                        data: Theme.of(context).copyWith(
                          colorScheme: ColorScheme.light(
                            primary: Color(0xff78b856),
                            onPrimary: Colors.white,
                            onSurface: Colors.black,
                          ),
                          textButtonTheme: TextButtonThemeData(
                            style: TextButton.styleFrom(
                              primary: Color(0xff78b856),
                            ),
                          ),
                        ),
                        child: child!,
                      );
                    },
                  );
                  if (newDate == null) return;
                  setState(() => date = newDate);
                },
                child: Icon(Icons.calendar_month),
                backgroundColor: const Color(0xff78b856),
              ),
              Padding(
                padding: EdgeInsets.all(16.0),
                child: Card(
                  child: DataTable(
                    columnSpacing: 0,
                    columns: [
                      DataColumn(
                          label: Text('Proizvodnja',
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.bold))),
                      DataColumn(
                          label: Text('Planirana',
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.bold))),
                      DataColumn(
                          label: Text('Ustvarjena',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.bold))),
                      DataColumn(
                          label: Text('Realizacija (%)',
                              style: TextStyle(
                                  fontSize: 10, fontWeight: FontWeight.bold))),
                    ],
                    rows: [
                      DataRow(cells: [
                        DataCell(Text('Dnevna')),
                        DataCell(Container(
                            alignment: Alignment.centerRight, child: Text('6,61'))),
                        DataCell(Container(
                            alignment: Alignment.centerRight, child: Text('4,49'))),
                        DataCell(Container(
                            alignment: Alignment.centerRight, child: Text('68'))),
                      ]),
                      DataRow(cells: [
                        DataCell(Text('Mesečna')),
                        DataCell(Container(
                            alignment: Alignment.centerRight,
                            child: Text('204,94'))),
                        DataCell(Container(
                            alignment: Alignment.centerRight,
                            child: Text('135,24'))),
                        DataCell(Container(
                            alignment: Alignment.centerRight, child: Text('66'))),
                      ]),
                      DataRow(cells: [
                        DataCell(Text('Letna')),
                        DataCell(Container(
                            alignment: Alignment.centerRight,
                            child: Text('2.293,48'))),
                        DataCell(Container(
                            alignment: Alignment.centerRight,
                            child: Text('2.891,26'))),
                        DataCell(Container(
                            alignment: Alignment.centerRight, child: Text('104'))),
                      ]),
                    ],
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.all(25.0),
                  child: FutureBuilder(
                      future: futureTabela,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return SfCartesianChart(
                              legend: Legend(isVisible: true),
                              tooltipBehavior: _tooltipBehavior,
                              zoomPanBehavior: ZoomPanBehavior(
                                enablePinching: true,
                                zoomMode: ZoomMode.x,
                                enablePanning: true,
                              ),
                              primaryXAxis: CategoryAxis(
                                interval: 1,
                                edgeLabelPlacement: EdgeLabelPlacement.shift,
                                visibleMaximum: 30,
                              ),
                              // Chart title
                              title: ChartTitle(text: 'Graf proizvodnje DEM'),
                              series: <ChartSeries<TabelaMariborskiOtok, String>>[
                                LineSeries<TabelaMariborskiOtok, String>(
                                  color: Color(0xff78b856),
                                  dataSource: chartData,
                                  xValueMapper: (TabelaMariborskiOtok date, _) =>
                                      date.date.toString(),
                                  yValueMapper:
                                      (TabelaMariborskiOtok production, _) =>
                                      (production.production * 0.001)
                                          .roundToDouble(),
                                  name: 'Produkcija',
                                  markerSettings: MarkerSettings(
                                    isVisible: false,
                                  ),
                                ),
                                LineSeries<TabelaMariborskiOtok, String>(
                                  color: Color(0xff4095d2),
                                  dataSource: chartData,
                                  dashArray: <double>[3, 3],
                                  xValueMapper: (TabelaMariborskiOtok date, _) =>
                                      date.date.toString(),
                                  yValueMapper: (TabelaMariborskiOtok plan, _) =>
                                      (plan.plan * 0.001).roundToDouble(),
                                  name: 'Plan',
                                  markerSettings: MarkerSettings(
                                    isVisible: false,
                                  ),
                                ),
                              ]);
                        }
                        return Card();
                      })),
              Padding(
                padding: EdgeInsets.all(25.0),
                child: Center(
                  child: ToggleSwitch(
                    minWidth: 90.0,
                    minHeight: 40.0,
                    fontSize: 16.0,
                    initialLabelIndex: stevio,
                    activeBgColor: [Color(0xff78b856)],
                    activeFgColor: Colors.white,
                    inactiveBgColor: Colors.white,
                    inactiveFgColor: Colors.grey,
                    totalSwitches: 3,
                    labels: ['Dnevno', 'Mesečno', 'Letno'],
                    onToggle: (index) {
                      sortiraj(index);
                    },
                  ),
                ),
              ),
              Center(
                child: Visibility(
                    visible: vidnoDnevno,
                    child: Card(
                      child: Dnevno.tabelaDnevno(),
                    )),
              ),
              Center(
                child: Visibility(
                    visible: vidnoMesecno,
                    child: Card(
                      child: Mesecno.tabelaMesecno(),
                    )),
              ),
              Center(
                child: Visibility(
                    visible: vidnoLetno,
                    child: Card(
                      child: Letno.tabelaLetno(),
                    )),
              ),
            ],
          ),
        ));
  }
}

class TabelaMariborskiOtok {
  TabelaMariborskiOtok(
      {required this.date, required this.production, required this.plan});

  final String date;
  final double production;
  final double plan;

  factory TabelaMariborskiOtok.fromJson(List<dynamic> parsedJson, int index) {
    return TabelaMariborskiOtok(
      date: parsedJson[index]['date'].toString(),
      production: double.parse(parsedJson[index]['production__kwh']),
      plan: parsedJson[index]['production_plan__kwh'],
    );
  }
}
