import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:neki/table.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'tabela.dart' as Tabela;
import 'package:http/http.dart' as http;


class Graf extends StatefulWidget {
  // ignore: prefer_const_constructors_in_immutables
  Graf();

  @override
  _GrafState createState() => _GrafState();
}

class _GrafState extends State<Graf> {
  late TooltipBehavior _tooltipBehavior;
  List<TabelaMariborskiOtok> chartData = [];

  late Future<TabelaMariborskiOtok> futureTabela;

  Future<TabelaMariborskiOtok> fetchTabela() async {
    final response = await http.get(Uri.parse('http://studentdocker.informatika.uni-mb.si:5005/Dnevni'));

    var usersData = jsonDecode(response.body);
    List<dynamic> tabelaJSON = List<dynamic>.from(usersData);



    for (int i = 0; i < tabelaJSON.length; i++) {
      chartData.add(TabelaMariborskiOtok.fromJson(tabelaJSON, i));

      TabelaMariborskiOtok.fromJson(tabelaJSON, i);
    }

    return TabelaMariborskiOtok(
        date: "month", production: 10000000, plan: 5000000000000);
  }

  @override
  void initState() {
    super.initState();
    _tooltipBehavior = TooltipBehavior(enable: true);
    futureTabela = fetchTabela();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Podatki"),
          backgroundColor: Color(0xff3c4e59),
          automaticallyImplyLeading: false,
          centerTitle: true,
        ),
        body: Center(
            child: FutureBuilder(
                future: futureTabela,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return SfCartesianChart(
                        legend: Legend(isVisible: true),
                        tooltipBehavior: _tooltipBehavior,
                        zoomPanBehavior: ZoomPanBehavior(
                          enablePinching: true,
                          zoomMode: ZoomMode.x,
                          enablePanning: true,
                        ),
                        primaryXAxis: CategoryAxis(
                          interval: 1,
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          visibleMaximum: 6,
                        ),
                        // Chart title
                        title: ChartTitle(text: 'Produkcijski plan'),
                        series: <ChartSeries<TabelaMariborskiOtok, String>>[
                          LineSeries<TabelaMariborskiOtok, String>(
                            color: Color(0xff78b856),
                            dataSource: chartData,
                            xValueMapper: (TabelaMariborskiOtok date, _) =>
                                date.date.toString(),
                            yValueMapper:
                                (TabelaMariborskiOtok production, _) =>
                                    production.production.toDouble(),
                            name: 'Produkcija',
                            markerSettings: MarkerSettings(
                              isVisible: false,
                            ),
                          ),
                          LineSeries<TabelaMariborskiOtok, String>(
                            color: Color(0xff4095d2),
                            dataSource: chartData,
                            dashArray: <double>[3, 3],
                            xValueMapper: (TabelaMariborskiOtok date, _) =>
                                date.date.toString(),
                            yValueMapper: (TabelaMariborskiOtok plan, _) =>
                                plan.plan.toDouble(),
                            name: 'Plan',
                            markerSettings: MarkerSettings(
                              isVisible: false,
                            ),
                          ),
                        ]);
                  }
                  return Card();
                })));
  }
}

class TabelaMariborskiOtok {
  TabelaMariborskiOtok(
      {required this.date, required this.production, required this.plan});
  final String date;
  final double production;
  final double plan;
  factory TabelaMariborskiOtok.fromJson(List<dynamic> parsedJson, int index) {
    return TabelaMariborskiOtok(
      date: parsedJson[index]['date'].toString(),
      production: double.parse(parsedJson[index]['production__kwh']),
      plan: parsedJson[index]['production_plan__kwh'],
    );
  }
}
